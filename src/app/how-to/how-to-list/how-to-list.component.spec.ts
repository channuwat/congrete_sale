import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HowToListComponent } from './how-to-list.component';

describe('HowToListComponent', () => {
  let component: HowToListComponent;
  let fixture: ComponentFixture<HowToListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HowToListComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HowToListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
