import { Component, OnInit, Input } from '@angular/core';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player/ngx';

@Component({
  selector: 'app-how-to-list',
  templateUrl: './how-to-list.component.html',
  styleUrls: ['./how-to-list.component.scss'],
})
export class HowToListComponent implements OnInit {

  @Input() clipData : any
  constructor(public youtube : YoutubeVideoPlayer) { }

  ngOnInit() {
    setTimeout(() => {
      console.log(this.clipData);
    }, 1000);
  }

  openYoutube(url){
    this.youtube.openVideo(url)
    
  }

}
