import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HowToPageRoutingModule } from './how-to-routing.module';

import { HowToPage } from './how-to.page';
import { HowToListComponent } from './how-to-list/how-to-list.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule.forChild(),
    HowToPageRoutingModule
  ],
  declarations: [HowToPage,HowToListComponent]
})
export class HowToPageModule {}
