import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { WebapiServiceProvider } from '../providers/webapi-service/webapi-service';

@Component({
  selector: 'app-how-to',
  templateUrl: './how-to.page.html',
  styleUrls: ['./how-to.page.scss'],
})
export class HowToPage implements OnInit {

  constructor(
    public api: WebapiServiceProvider,
    public ref: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.loadClip()
  }

  public clip = []
  loadClip() {
    this.clip = []
    this.api.getData('getManual').then((res: any) => {
      res.forEach(el => {
        var data = {
          title_th: el.mn_title.th,
          title_en: el.mn_title.en,
          url: el.short_link
        }
        this.clip.push(data)
      });
    })
  }

  async doRefresh(event) {
    await this.loadClip()
    await event.target.complete();
  }

}
