import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddBookBankPage } from './add-book-bank.page';

describe('AddBookBankPage', () => {
  let component: AddBookBankPage;
  let fixture: ComponentFixture<AddBookBankPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddBookBankPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddBookBankPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
