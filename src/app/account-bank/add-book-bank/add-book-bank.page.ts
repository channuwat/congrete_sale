import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { BankService } from '../bank.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { WebapiServiceProvider } from 'src/app/providers/webapi-service/webapi-service';
import { Storage } from '@ionic/storage';
import { async } from '@angular/core/testing';

@Component({
  selector: 'app-add-book-bank',
  templateUrl: './add-book-bank.page.html',
  styleUrls: ['./add-book-bank.page.scss'],
})
export class AddBookBankPage implements OnInit {

  public fromGroupB: FormGroup
  public bank = []
  public user: any
  public lang = 'th'
  constructor(
    public modalCtr: ModalController,
    public Bank: BankService,
    public service: WebapiServiceProvider,
    public store: Storage
  ) {
    this.store.get('data_login').then((data: any) => {
      this.user = data
    })

    this.service.storage_get('lang').then((lang_data) => {
      this.lang = lang_data
    })

    this.bank = Bank.bankList()
    this.fromGroupB = new FormGroup({
      'type_bank': new FormControl('', [Validators.required]),
      'bank': new FormControl('', [Validators.required]),
      'bank_account': new FormControl('', [Validators.required, Validators.pattern("[0-9]{1,15}")]),
      'bank_name': new FormControl('', [Validators.required])
    })
  }

  ngOnInit() {
  }


  async save() {
    var data: any = await {
      user: this.user.member_id,
      type_bank: this.fromGroupB.value.type_bank,
      bank: this.fromGroupB.value.bank,
      bank_account: this.fromGroupB.value.bank_account.trim(),
      bank_name: this.fromGroupB.value.bank_name.trim(),
    }

    var bank = this.fromGroupB.value.bank
    await this.bank.forEach((el) => {
      if (bank == el.name) {
        data.logo = el.logo
      }
    });

    await this.service.postData('addBookBank', data).then((res: any) => {
      this.close()
    })

  }

  close() {
    this.modalCtr.dismiss()
  }

}
