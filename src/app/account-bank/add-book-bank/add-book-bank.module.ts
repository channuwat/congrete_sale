import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddBookBankPageRoutingModule } from './add-book-bank-routing.module';

import { AddBookBankPage } from './add-book-bank.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    TranslateModule.forChild(),
    AddBookBankPageRoutingModule
  ],
  declarations: [AddBookBankPage]
})
export class AddBookBankPageModule {}
