import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddBookBankPage } from './add-book-bank.page';

const routes: Routes = [
  {
    path: '',
    component: AddBookBankPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddBookBankPageRoutingModule {}
