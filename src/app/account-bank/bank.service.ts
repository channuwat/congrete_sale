import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BankService {

  constructor() { }

  bankList() {
    var bank = [
      { name: { th: 'ธ.กรุงเทพ', en: 'BBL' }, logo: '../../../assets/images/logo_bank/bbl.png' },
      { name: { th: 'ธ.กสิกรไทย', en: 'KBANK' }, logo: '../../../assets/images/logo_bank/kb.png' },
      { name: { th: 'ธ.กรุงไทย', en: 'KTB' }, logo: '../../../assets/images/logo_bank/ktb.png' },
      { name: { th: 'ธ.ทหารไทย', en: 'TMB' }, logo: '../../../assets/images/logo_bank/tmb.png' },
      { name: { th: 'ธ.ไทยพาณิชย์', en: 'SCB' }, logo: '../../../assets/images/logo_bank/scb.png' },
      { name: { th: 'ธ.กรุงศรีอยุธยา', en: 'BAY' }, logo: '../../../assets/images/logo_bank/bay.png' },
      { name: { th: 'ธ.เกียรตินาคิน', en: 'KKP' }, logo: '../../../assets/images/logo_bank/ksb.png' },
      { name: { th: 'ธ.ซีไอเอ็มบีไทย', en: 'CIMB' }, logo: '../../../assets/images/logo_bank/cimb.png' },
      { name: { th: 'ธ.ทิสโก้', en: 'TISCO' }, logo: '../../../assets/images/logo_bank/tisco.png' },
      { name: { th: 'ธ.ธนชาต', en: 'TBANK' }, logo: '../../../assets/images/logo_bank/tb.png' },
      { name: { th: 'ธ.ยูโอบี', en: 'UOB' }, logo: '../../../assets/images/logo_bank/uob.png' },
      { name: { th: 'ธ.ออมสิน', en: 'GSB' }, logo: '../../../assets/images/logo_bank/gsb.png' },
      { name: { th: 'ธ.อาคารสงเคราะห์', en: 'GHB' }, logo: '../../../assets/images/logo_bank/ghb.png' },
      { name: { th: 'ธ.อิสลามแห่งประเทศไทย', en: 'iBank' }, logo: '../../../assets/images/logo_bank/ibb.png' },
      { name: { th: 'ธ.ธกส', en: 'BAAC' }, logo: '../../../assets/images/logo_bank/boa.png' },
    ]
    return bank
  }

}
