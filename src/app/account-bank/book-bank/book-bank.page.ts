import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { AddBookBankPage } from '../add-book-bank/add-book-bank.page';
import { Storage } from '@ionic/storage';
import { WebapiServiceProvider } from 'src/app/providers/webapi-service/webapi-service';
import { Location } from '@angular/common';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-book-bank',
  templateUrl: './book-bank.page.html',
  styleUrls: ['./book-bank.page.scss'],
})
export class BookBankPage implements OnInit {

  public user: any = []
  public personalBank: any = []
  public organizBank: any = []
  public bankList: any = []
  constructor(
    public route: Router,
    public modalCtr: ModalController,
    public store: Storage,
    public service: WebapiServiceProvider,
    public location: Location,
    public alert: AlertController,
    public ref: ChangeDetectorRef
  ) {

  }


  ngOnInit() {
    this.store.get('data_login').then((res: any) => {
      this.user = res
      this.loadList(this.user.member_id)
    })
  }

  public lang: string = 'th'
  ionViewWillEnter() {
    this.service.storage_get('lang').then((lang_res) => {
      this.lang = lang_res
    })
  }

  loadList(member_id) {
    this.service.getData('selectUserBank/' + member_id).then((list: any) => {
      this.personalBank = []
      this.organizBank = []
      this.bankList = list
      for (let bank of list) {
        if (bank.type_bank == 1) {
          this.personalBank.push(bank)
        } else {
          this.organizBank.push(bank)
        }
      }
      this.ref.detectChanges()
    })
  }

  updateState(check, bb_id, member_id, type) {
    var data = {
      check: check,
      bb_id: bb_id,
      member_id: member_id,
      type: type
    }
    this.service.postData('updateStateBank', data).then((error: any) => {
      this.loadList(this.user.member_id)
    })
  }

  async deleteBookbannk(check, bb_id, member_id, type) {
    var data = {
      check: check,
      bb_id: bb_id,
      member_id: member_id,
      type: type
    }
    var langObj = {
      th: {
        header: 'ลบบัญชีธนาคาร',
        btn_txtCC: 'ยกเลิก',
        btn_txtOK: 'ยืนยัน',
        alert: 'ลบสำเร็จ'
      },
      en: {
        header: 'Delete Bookbank',
        btn_txtCC: 'Cancel',
        btn_txtOK: 'Confirm',
        alert: 'Deleted Success'
      }
    }
    let alertLang = langObj.th
    if (this.lang == 'en') {
      alertLang = langObj.en
    }
    const alert = await this.alert.create({
      cssClass: 'my-custom-class',
      header: alertLang.header,
      mode: 'ios',
      buttons: [
        {
          text: alertLang.btn_txtCC,
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            this.loadList(this.user.member_id)
          }
        }, {
          text: alertLang.btn_txtOK,
          handler: () => {
            this.service.postData('updateStateBank', data).then(async (error: any) => {
              if (error == 'deleted') {
                await this.service.Toast(alertLang.alert, 2500, 'bottom')
                this.loadList(this.user.member_id)
              }
            })
          }
        }
      ]
    });
    await alert.present();
  }

  copyBookbank(bb_no: string) {
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = bb_no;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    let copy = 'คัดลอกเลขบัญชีธนาคาร '
    if(this.lang == 'en'){
      copy = 'Bank account number copied. '
    }
    this.service.Toast(copy + bb_no, 3000, 'bottom')
  }

  doRefresh(event) {
    setTimeout(() => {
      this.loadList(this.user.member_id)
      this.ref.detectChanges()
      event.target.complete();
    }, 1000);
  }

  async addBookBank() {
    const modal = await this.modalCtr.create({
      component: AddBookBankPage
    })

    modal.onWillDismiss().then((d) => {
      this.loadList(this.user.member_id)
    })

    await modal.present()
  }

  close() {
    this.location.back()
    //this.route.navigate(['../main/account'])
  }

}
