import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BookBankPageRoutingModule } from './book-bank-routing.module';

import { BookBankPage } from './book-bank.page';
import { AddBookBankPage } from '../add-book-bank/add-book-bank.page';
import { AddBookBankPageModule } from '../add-book-bank/add-book-bank.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BookBankPageRoutingModule,
    TranslateModule.forChild(),
    AddBookBankPageModule
  ],
  declarations: [BookBankPage],
  entryComponents : [AddBookBankPage]
})
export class BookBankPageModule {}
