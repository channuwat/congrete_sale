import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BookBankPage } from './book-bank.page';

const routes: Routes = [
  {
    path: '',
    component: BookBankPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BookBankPageRoutingModule {}
