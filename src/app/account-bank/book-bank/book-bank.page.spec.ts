import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BookBankPage } from './book-bank.page';

describe('BookBankPage', () => {
  let component: BookBankPage;
  let fixture: ComponentFixture<BookBankPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookBankPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BookBankPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
