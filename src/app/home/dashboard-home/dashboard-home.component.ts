import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { WebapiServiceProvider } from 'src/app/providers/webapi-service/webapi-service';
import { ModalController } from '@ionic/angular';
import { AuctionPage } from 'src/app/auction/auction.page';

@Component({
  selector: 'app-dashboard-home',
  templateUrl: './dashboard-home.component.html',
  styleUrls: ['./dashboard-home.component.scss'],
})
export class DashboardHomeComponent implements OnInit {
  public data = { member_id: 0, phone: '', name: '', email: '', address: '', picture: '', approve: '-1' };
  public time_ViewDidEnter = 0
  constructor(
    public router: Router,
    public api: WebapiServiceProvider,
    private cd: ChangeDetectorRef,
    public modalController: ModalController
  ) {
    this.api.fb_val('order', () => {
      this.api.storage_get('data_login').then((data: any) => {
        this.data = data
        this.loadOrderOut(data.member_id, 3)
        this.loadOrderIn(data.member_id, 3)
        this.api.withoutStoreUser(data.member_id).then((user: any) => {
          this.data = user.data
        })
      });
    })
    setInterval(() => {
      this.time_ViewDidEnter++;
    }, 1000)
  }

  ngOnInit() { }

  public new_order = [];
  loadOrderOut(member_id, list) {
    this.api.getData('order_data_out/0/' + member_id + '/' + list).then((res: any) => {
      this.new_order = res;
      this.cd.detectChanges();
    });
  }

  public order_in = [];
  loadOrderIn(member_id, list) {
    this.api.getData('order_data_in/0/' + member_id + '/' + list).then((res: any) => {
      this.order_in = res;
      this.cd.detectChanges();
    });
  }

  async select(id, type) {
    const modal = await this.modalController.create({
      component: AuctionPage,
      componentProps: {
        order_data: { order_id: id, auct_type: type }
      }
    });
    modal.onDidDismiss().then((res: any) => {
    });
    return await modal.present();
  }

}
