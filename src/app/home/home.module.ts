import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomePageRoutingModule } from './home-routing.module';

import { HomePage } from './home.page';
import { ComponentsModule } from '../components/components.module';
import { TranslateModule } from '@ngx-translate/core';
//import { VerifyComponentModule } from '../verify-component/verify-component.module';
import { DashboardHomeComponent } from './dashboard-home/dashboard-home.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule,
    ComponentsModule,
    TranslateModule.forChild(),
    //VerifyComponentModule
  ],
  declarations: [HomePage,DashboardHomeComponent],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class HomePageModule {}
