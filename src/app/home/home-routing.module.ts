import { NgModule } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';

import { HomePage } from './home.page';
import { Storage } from '@ionic/storage';
import { WebapiServiceProvider } from '../providers/webapi-service/webapi-service';

const routes: Routes = [
  {
    path: '',
    component: HomePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomePageRoutingModule {
  constructor(){
    // public storage: Storage, public router: Router, public api: WebapiServiceProvider
    // this.storage.get('data_login').then((data: any) => {
    //   this.api.withoutStoreUser(data.member_id).then((user: any) => {
    //     user = user.data
    //     if (user) {
    //       if (user.approve == 0) {
    //         this.router.navigate(['/unknow']);
    //       } else {
    //         this.router.navigate(['/main/home']);
    //       }
    //     } else {
    //       this.router.navigate(['/auth/login']);
    //     }
    //   })
    // });
  }
}
