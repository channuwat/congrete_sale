import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { WebapiServiceProvider } from '../providers/webapi-service/webapi-service';
import { ActionSheetController, Events, LoadingController, Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { Device } from '@ionic-native/device/ngx';
import { Location } from '@angular/common';

@Component({
  selector: 'app-account',
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.scss'],
})
export class AccountPage implements OnInit {

  constructor(public router: Router,
    public actionSheetController: ActionSheetController,
    public store: Storage,
    public api: WebapiServiceProvider,
    public translate: TranslateService,
    public device: Device,
    public location: Location,
    public loadingController : LoadingController,
    public events: Events
  ) { }
  public data = { member_id: 0, phone: '', name: '', email: '', address: '', picture: '' };
  public point = 0
  ngOnInit() {
  }

  ionViewWillEnter() {
    // กำหนด uri ลง Store //
    this.api.lastLocationURI('set')
    this.api.storage_get('data_login').then((data: any) => {
      this.data = data;
      this.api.getData('get_score/' + this.data.member_id).then((score: any) => {
        this.point = score.score
      })
    });
  }

  getScore(member_id) {
    return this.point
  }

  async change_lang() {
    const actionSheet = await this.actionSheetController.create({
      header: 'เลือกภาษา (Language)',
      buttons: [{
        text: 'ไทย',
        role: 'destructive',
        // icon: 'trash',
        handler: () => {
          //this.translate.use('th')
          this.store.set('lang', 'th')
          this.presentLoading('th');
        }
      }, {
        text: 'ENG',
        // icon: 'share',
        handler: () => {
          //this.translate.use('en')
          this.store.set('lang', 'en')
          this.presentLoading('en');
        }
      }, {
        text: 'ปิดออก (Close)',
        // icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }],
      mode : "ios"
    });
    await actionSheet.present();
  }

  async presentLoading(lang) {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...',
      mode:'ios'

    });
    await loading.present();
    await this.events.publish('user:lang', lang)
    await loading.dismiss();
  }

  forgotPassword() {
    this.store.set('otp_phone', this.data.phone)
    setTimeout(() => {
      this.router.navigate(['../../../auth/add-password/account']);
    }, 500);

  }

  logout() {
    this.api.storage_set('data_login', null);
    let uuid = this.device.uuid
    this.api.postData('logout', { uuid: uuid }).then(() => {
      setTimeout(() => {
        this.router.navigate(['/auth/login']);
      }, 100);
    })

  }

  close() {
    this.location.back()
  }

  doRefresh(event) {
    this.ionViewWillEnter()
    setTimeout(() => {
      event.target.complete();
    }, 1500);
  }

}
