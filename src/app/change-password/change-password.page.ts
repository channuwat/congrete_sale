import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PasswordValidator } from '../validators/password.validator';
import { Router } from '@angular/router';
import { WebapiServiceProvider } from '../providers/webapi-service/webapi-service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.page.html',
  styleUrls: ['./change-password.page.scss'],
})
export class ChangePasswordPage implements OnInit {

  matching_passwords_group: FormGroup;

  loginForm: FormGroup;

  validation_messages : any = {
    password: [
      { type: 'required', message: 'Password is required.' },
      { type: 'minlength', message: 'Password must be at least 6 characters long.' }
    ],
    confirm_password: [
      { type: 'required', message: 'Confirm Password is required.' },

    ],
    name: [
      { type: 'required', message: 'Address is required.' }
    ],
    email: [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'Not correct' }
    ]
  };
  constructor(public router: Router, public api: WebapiServiceProvider) {
    this.loginForm = new FormGroup({
      password: new FormControl('', Validators.compose([
        Validators.minLength(6),
        Validators.required
      ])),
      confirm_password: new FormControl('', Validators.compose([Validators.required])),
      name: new FormControl('', Validators.compose([Validators.required])),
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      address: new FormControl('')
    });
  }

  ngOnInit() {
    this.api.storage_get("lang").then((lang_res) => {
      if (lang_res == 'th') {
        this.validation_messages = {
          password: [
            { type: 'required', message: 'โปรดกรอกรหัสผ่าน' },
            { type: 'minlength', message: 'โปรดกรอกรหัสผ่านอย่างน้อย 6 ตัวอักษร' }
          ],
          confirm_password: [
            { type: 'required', message: 'โปรดกรอกรหัสผ่านยืนยัน' },

          ],
          name: [
            { type: 'required', message: 'โปรดกรอกชื่อ' }
          ],
          email: [
            { type: 'required', message: 'โปรดกรอกอีเมลล์' },
            { type: 'pattern', message: 'รูปแบบไม่ถูกต้อง' }
          ]
        };
      }
    })
  }

  doLogin() {
    console.log(this.loginForm.value);
    this.api.storage_get('otp_phone').then((val: any) => {
      let data = this.loginForm.value;
      data.phone = val;
      this.api.postData("add_password", data).then((result: any) => {
        console.log(result);
        if (result.flag == '1') {
          if(result.error == 'inserted'){
            this.api.fb_set('alert/menu/saller')
          }
          this.api.Toast("บันทึกข้อมูลเรียบร้อยแล้ว", 2500, 'top');
          setTimeout(() => {
            this.router.navigate(['auth/login']);
          }, 1000);
        } else {
          if (result.error == 'password') {
            this.api.Toast("รหัสผ่านไม่ตรงกัน", 2500, 'top');
            this.loginForm.patchValue({ confirm_password: null })
          } else {
            this.api.Toast("อีเมล์นี้ถูกใช้เเล้ว", 2500, 'top');
            this.loginForm.patchValue({ email: null })
          }
        }
      });
    });

  }

}
