import { Component, ChangeDetectorRef } from '@angular/core';
import { WebapiServiceProvider } from '../providers/webapi-service/webapi-service';
import { AngularFireDatabase } from '@angular/fire/database';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { Events } from '@ionic/angular';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  public user: any = { member_id: '' }
  public statePage = false
  constructor(
    public service: WebapiServiceProvider,
    public firebase: AngularFireDatabase,
    public store: Storage,
    public ref: ChangeDetectorRef,
    public route: Router,
    public events: Events
  ) {
    // this.events.subscribe('user:login', async (res) => {
    //   if (res == 'success') {
    //     console.log(res);
    //     await this.ionViewWillEnter();
    //     await this.ionViewDidEnter();
    //   }
    // });

    this.events.subscribe('user:lang', (res) => {
      setTimeout(() => {
        this.ref.detectChanges();
      }, 1000);
    });
  }

  ionViewWillEnter() {
    console.log('tabs');
    this.store.get('data_login').then((res: any) => {
      if (!res) {
        this.route.navigate(['auth/login']);
      }
    })
  }


  ionViewDidEnter() {
    this.statePage = true
    this.store.get('data_login').then((data: any) => {
      if (data) {
        this.user = data
        this.service.fb_val('notification/user_' + this.user.member_id + '/chat', (res) => {
          if (this.statePage) {
            console.log(res);
            
            this.readC = res
            this.ref.detectChanges()
          }
        });
        this.service.fb_val('notification/user_' + this.user.member_id + '/alert', (res) => {
          if (this.statePage) {
            this.readA = res
            this.ref.detectChanges()
          }
        });
      }
    })
  }

  ionViewWillLeave() {
    this.statePage = false
  }

  public readC = 0
  public readA = 0
  clickReaded(read, type) {
    if (read > 0) {
      if (type == 'chat') {
        this.service.fb_set_alert('notification/user_' + this.user.member_id + '/chat')
      } else {
        this.service.fb_set_alert('notification/user_' + this.user.member_id + '/alert')
      }
    }
    this.ref.detectChanges()
  }



}
