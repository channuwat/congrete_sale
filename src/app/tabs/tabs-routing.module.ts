import { NgModule } from '@angular/core';
import { RouterModule, Routes, Router } from '@angular/router';
import { TabsPage } from './tabs.page';
import { Storage } from '@ionic/storage';
import { WebapiServiceProvider } from '../providers/webapi-service/webapi-service';
import { async } from '@angular/core/testing';
const routes: Routes = [
  {
    path: 'main',
    component: TabsPage,
    children: [
      {
        path: 'tab2',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../tab2/tab2.module').then(m => m.Tab2PageModule)
          }
        ]
      },
      {
        path: 'step1',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../tab1/tab1.module').then(m => m.Tab1PageModule)
          }
        ]
      },
      {
        path: 'tab2',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../tab2/tab2.module').then(m => m.Tab2PageModule)
          }
        ]
      },
      {
        path: 'tab3',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../tab3/tab3.module').then(m => m.Tab3PageModule)
          }
        ]
      },
      {
        path: 'how',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../how-to/how-to.module').then(m => m.HowToPageModule)
          }
        ]
      },
      {
        path: 'account',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../account/account.module').then(m => m.AccountPageModule)
          },
          {
            path: 'config-account',
            loadChildren: () =>
              import('../config-account/config-account.module').then(m => m.ConfigAccountPageModule)
          },
          {
            path: 'setting-location-order',
            loadChildren: () =>
              import('../setting-location-order/setting-location-order.module').then(m => m.SettingLocationOrderPageModule)
          },
          {
            path: 'book-bank',
            loadChildren: () =>
              import('../account-bank/book-bank/book-bank.module').then(m => m.BookBankPageModule)
          },
          {
            path: 'verify',
            loadChildren: () =>
              import('../verify/verify.module').then(m => m.VerifyPageModule)
          },
          {
            path: 'score',
            loadChildren: () =>
              import('../score/score.module').then(m => m.ScorePageModule)
          }
        ]
      },
      {
        path: 'notification',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../notification/notification.module').then(m => m.NotificationPageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/main/tab2/order-wait',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/main/tab2/order-wait',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {
  constructor(
    public storage: Storage, public router: Router, public api: WebapiServiceProvider
  ) {
    this.storage.get('walkthrough').then((wal: any) => {
      if (wal) {
        this.storage.get('data_login').then((data: any) => {
          if (data) {
            this.api.checkUserUnknow().then((res) => {
              let unknow = res
              if (unknow == 0) {
                this.router.navigate(['unknow'])
              } 
            })
            //await this.router.navigate(['/main/home']);
          } else {
            this.router.navigate(['/auth/login']);
          }
        });
      } else {
        this.router.navigate(['/walkthrough']);
      }
    })
  }

}
