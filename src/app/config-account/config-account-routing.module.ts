import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConfigAccountPage } from './config-account.page';

const routes: Routes = [
  {
    path: '',
    component: ConfigAccountPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConfigAccountPageRoutingModule {}
