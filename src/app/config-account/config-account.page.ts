import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { WebapiServiceProvider } from '../providers/webapi-service/webapi-service';
import { Storage } from '@ionic/storage';
import { AlertController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-config-account',
  templateUrl: './config-account.page.html',
  styleUrls: ['./config-account.page.scss'],
})
export class ConfigAccountPage implements OnInit {
  matching_passwords_group: FormGroup;

  loginForm: FormGroup;

  validation_messages = {
    name: [
      { type: 'required', message: 'Address is required.' }
    ],
    email: [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'รูปแบบไม่ถูกต้อง' }
    ],
    address: [
      { type: 'required', message: 'Address is required.' }
    ]
  };

  public member: any = 0
  constructor(
    public route: Router,
    public router: ActivatedRoute,
    public store: Storage,
    public service: WebapiServiceProvider,
    public alert: AlertController,
  ) {
    this.member = this.router.snapshot.paramMap.get('member_id')
    this.loginForm = new FormGroup({
      name: new FormControl('', Validators.compose([Validators.required])),
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      address: new FormControl('', Validators.compose([]))
    });
  }

  ngOnInit() {
    console.log(this.loginForm);

  }

  ionViewWillEnter() {
    this.loadData()
  }

  public acc: any = {}
  loadData() {
    this.service.getData('getDataAccount/' + this.member).then((dataAcc: any) => {
      console.log(dataAcc);

      this.loginForm.patchValue({
        name: dataAcc.name,
        email: dataAcc.email,
        address: dataAcc.address
      })
      this.url = dataAcc.picture
      this.acc.member_id = dataAcc.member_id
      this.store.set('data_login', dataAcc)
    })
  }

  public imagePath: any = '';
  public url = '';
  async uploadProfile(e) {
    // e.preventDefault();
    // console.log(e);
    let el: HTMLElement = document.getElementById('inputFile') as HTMLElement;
    el.click();
  }
  public formData = new FormData();
  public reader: any
  public typeIMG: any
  changeListener(event): void {

    this.formData.append('avatar', event.target.files[0]);
    this.reader = new FileReader();
    this.reader.onload = (event: any) => {
      this.url = event.target.result;
    }
    this.reader.readAsDataURL(event.target.files[0]);
    this.typeIMG = event.target.files[0].type

  }

  async save() {
    const alert = await this.alert.create({
      cssClass: 'my-custom-class',
      message: 'ยืนยันการเเก้ไข',
      buttons: [
        {
          text: 'ยกเลิก',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            // some code
          }
        }, {
          text: 'ยืนยัน',
          handler: () => {
            var img: any
            var type: ''
            if (this.reader == undefined) {
              img = null
              type = ''
            } else {
              img = this.reader.result
              if (this.typeIMG != '') {
                type = this.typeIMG
              }

            }

            this.acc.name = this.loginForm.value.name,
              this.acc.email = this.loginForm.value.email,
              this.acc.address = this.loginForm.value.address
            this.service.postData('configAccount', { input: this.acc, img: img, type: type }).then(() => {
              this.service.Toast('แก้ไขข้อมูลสำเร็จ', 2000, 'top')
              this.loadData()
            })
          }
        }
      ]
    });

    await alert.present();
  }

  close() {
    this.route.navigate(['../main/account'])
  }

}
