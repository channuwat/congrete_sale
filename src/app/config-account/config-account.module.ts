import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConfigAccountPageRoutingModule } from './config-account-routing.module';

import { ConfigAccountPage } from './config-account.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConfigAccountPageRoutingModule,
    TranslateModule.forChild(),
    ReactiveFormsModule
  ],
  declarations: [ConfigAccountPage],
  schemas : [CUSTOM_ELEMENTS_SCHEMA]
})
export class ConfigAccountPageModule {}
