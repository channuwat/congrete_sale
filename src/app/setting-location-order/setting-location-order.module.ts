import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SettingLocationOrderPageRoutingModule } from './setting-location-order-routing.module';

import { SettingLocationOrderPage } from './setting-location-order.page';
import { DistrictModalPage } from './district-modal/district-modal.page';
import { DistrictModalPageModule } from './district-modal/district-modal.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SettingLocationOrderPageRoutingModule,
    TranslateModule.forChild(),
    DistrictModalPageModule
    
  ],
  declarations: [SettingLocationOrderPage],
  entryComponents:[DistrictModalPage]
})
export class SettingLocationOrderPageModule {}
