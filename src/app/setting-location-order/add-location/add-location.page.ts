import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { WebapiServiceProvider } from 'src/app/providers/webapi-service/webapi-service';
import { Storage } from '@ionic/storage';
import { DistrictModalPage } from '../district-modal/district-modal.page';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-location',
  templateUrl: './add-location.page.html',
  styleUrls: ['./add-location.page.scss'],
})
export class AddLocationPage implements OnInit {

  constructor(
    public service: WebapiServiceProvider,
    public modal: ModalController,
    public store: Storage,
    public route: Router
  ) { }

  public data = { member_id: 0, phone: '', name: '', email: '', address: '' };
  ngOnInit() {
    this.service.storage_get('data_login').then((data: any) => {
      this.data = data;
    });
  }

  public lang = 'th'
  ionViewWillEnter() {
    this.service.storage_get('lang').then(async (lang_res) => {
      if (lang_res == 'en') {
        this.lang = 'en'
      }
      this.service.postData('getLocationPost/', { txt: 'ก' }).then((data: any) => {
        this.local_data = data
        this.local_data.forEach(val => {
          if (this.lang == 'th') {
            val.localName_std = val.local_name_th
          } else {
            val.localName_std = val.local_name_en
          }
        });
      })
    })
  }

  public local_data: any = []
  findLocation(e) {
    let provice = e.detail.value
    this.service.postData('getLocationPost/', { txt: provice }).then((data: any) => {
      this.local_data = data
      this.local_data.forEach(val => {
        if (this.lang == 'th') {
          val.localName_std = val.local_name_th
        } else {
          val.localName_std = val.local_name_en
        }
      });

    })
  }

  async showDistrict(obj) {
    console.log(obj);

    obj.member_id = this.data.member_id
    let modal = await this.modal.create({
      component: DistrictModalPage,
      componentProps: { obj: obj }
    })

    modal.onWillDismiss().then((error_txt) => {
      setTimeout(() => {
        //this.serviceProvince()
      }, 300);
    })

    await modal.present()
  }

  close() {
    this.route.navigate(['main/account/setting-location-order'])
  }
}
