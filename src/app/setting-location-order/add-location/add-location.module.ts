import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddLocationPageRoutingModule } from './add-location-routing.module';

import { AddLocationPage } from './add-location.page';
import { DistrictModalPage } from '../district-modal/district-modal.page';
import { DistrictModalPageModule } from '../district-modal/district-modal.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddLocationPageRoutingModule,
    TranslateModule.forChild(),
    DistrictModalPageModule
  ],
  declarations: [AddLocationPage],
  entryComponents:[DistrictModalPage]
})
export class AddLocationPageModule {}
