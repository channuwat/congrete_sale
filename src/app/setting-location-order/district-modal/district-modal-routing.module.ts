import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DistrictModalPage } from './district-modal.page';

const routes: Routes = [
  {
    path: '',
    component: DistrictModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DistrictModalPageRoutingModule {}
