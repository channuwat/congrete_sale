import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DistrictModalPage } from './district-modal.page';

describe('DistrictModalPage', () => {
  let component: DistrictModalPage;
  let fixture: ComponentFixture<DistrictModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DistrictModalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DistrictModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
