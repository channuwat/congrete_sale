import { Component, OnInit, Input } from '@angular/core';
import { WebapiServiceProvider } from 'src/app/providers/webapi-service/webapi-service';
import { Storage } from '@ionic/storage';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-district-modal',
  templateUrl: './district-modal.page.html',
  styleUrls: ['./district-modal.page.scss'],
})
export class DistrictModalPage implements OnInit {

  @Input() obj: any;
  constructor(
    public service: WebapiServiceProvider,
    public store: Storage,
    public modal: ModalController
  ) { }

  public data: any = []
  public district: any = { member_id: this.data.member_id, district_id: [] }
  ngOnInit() {
    this.service.fb_val('order', () => {
      this.service.storage_get('data_login').then((data: any) => {
        this.data = data;
        this.service.storage_get('district_tmp').then((d_data: any) => {
          if (d_data != null) {
            this.district = d_data
          } else {
            var data = {
              member_id: this.data.member_id,
              district_id: []
            }
            this.service.storage_set('district_tmp', data)
          }
        })
      });
    })

  }

  public lang = 'th'
  ionViewWillEnter() {
    this.service.storage_get('lang').then(async (lang_res) => {
      if (lang_res == 'en') {
        this.lang = 'en'
      }
      this.setDistrict()
    })
  }

  ionViewDidEnter() {
  }

  public district_obj: any = []
  public obj_temp: any = []
  setDistrict() {
    this.service.getData('getDistrictSetting/' + this.obj.key_local + '/' + this.obj.member_id).then((district: any) => {
      this.obj.data_obj.forEach((el_1, index) => {
        if (this.lang == 'th') {
          el_1.am_name_std = el_1.am_name_th
        }else{
          el_1.am_name_std = el_1.am_name_en
        }
        el_1.isChecked = false
        district.forEach(el_2 => {
          if (el_1.am_id == el_2.id) {
            el_1.isChecked = true
          }
        });
      });
      this.obj_temp = this.obj.data_obj
    })
  }

  changeSelectAll(e) {
    let check = e.detail.checked

    if (check) {
      this.obj.data_obj.forEach((element, index) => {
        element.isChecked = true

      });
    } else {
      this.obj.data_obj.forEach((element, index) => {
        element.isChecked = false
      });
    }
  }

  save() {
    console.log(this.obj_temp);
    this.service.postData('addObjAddress', { district: this.obj_temp, member_id: this.data.member_id }).then((data: any) => {
      this.service.fb_set('order')
    }).then(() => {
      this.service.Toast('บันทึกสถานที่จัดส่งสำเร็จ', 2000, 'bottom')
      setTimeout(() => {
        this.close()
      }, 2500);
    })
  }

  close() {
    this.modal.dismiss({ value: 'xxx' });
  }
}
