import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SettingLocationOrderPage } from './setting-location-order.page';

describe('SettingLocationOrderPage', () => {
  let component: SettingLocationOrderPage;
  let fixture: ComponentFixture<SettingLocationOrderPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingLocationOrderPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SettingLocationOrderPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
