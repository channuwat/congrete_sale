import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SettingLocationOrderPage } from './setting-location-order.page';

const routes: Routes = [
  {
    path: '',
    component: SettingLocationOrderPage
  },
  {
    path: 'district-modal',
    loadChildren: () => import('./district-modal/district-modal.module').then( m => m.DistrictModalPageModule)
  },
  {
    path: 'add-location',
    loadChildren: () => import('./add-location/add-location.module').then( m => m.AddLocationPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SettingLocationOrderPageRoutingModule {}
