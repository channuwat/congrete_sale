import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { WebapiServiceProvider } from '../providers/webapi-service/webapi-service';
import { ModalController } from '@ionic/angular';
import { DistrictModalPage } from './district-modal/district-modal.page';
import { Storage } from '@ionic/storage';
import { AddLocationPage } from './add-location/add-location.page';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-setting-location-order',
  templateUrl: './setting-location-order.page.html',
  styleUrls: ['./setting-location-order.page.scss'],
})
export class SettingLocationOrderPage implements OnInit {

  constructor(
    public service: WebapiServiceProvider,
    public modal: ModalController,
    public store: Storage,
    public ref: ChangeDetectorRef,
    public route: Router,
    public router: ActivatedRoute,
    public location: Location
  ) { }

  public findProvince = ''
  public data = { member_id: 0, phone: '', name: '', email: '', address: '' };
  ngOnInit() {
    this.service.storage_get('data_login').then((data: any) => {
      this.data = data;
    });
  }

  public lang = 'th'
  ionViewDidEnter() {
    this.service.storage_get('lang').then(async (lang_res) => {
      if (lang_res == 'en') {
        this.lang = 'en'
      }
      this.serviceProvince()
    })

  }


  public province = []
  serviceProvince() {
    this.service.postData('provincePreview', { member_id: this.data.member_id }).then((pro_data: any) => {
      this.province = pro_data
      this.province.forEach(val => {
        if (this.lang == 'th') {
          val.localName_std = val.local_name_th
        }else{
          val.localName_std = val.local_name_en
        }
      });

    })
  }

  async openProvice(obj) {
    obj.member_id = this.data.member_id
    let modal = await this.modal.create({
      component: DistrictModalPage,
      componentProps: { obj: obj }
    })

    modal.onWillDismiss().then((error_txt) => {
      setTimeout(() => {
        this.serviceProvince()
      }, 300);
    })

    await modal.present()
  }

  close() {
    this.service.lastLocationURI('get')

    // var con_route: string = this.router.snapshot.paramMap.get('page')
    // if (con_route == 'account') {
    //this.route.navigate([this.service.lastLocationURI('get')])
    // } else {
    //   this.location.back();
    // }
  }
}
