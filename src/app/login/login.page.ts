import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { MenuController, ModalController } from '@ionic/angular';
import { WebapiServiceProvider } from '../providers/webapi-service/webapi-service';
import { AngularFireDatabase } from '@angular/fire/database';
import { FCMPluginOnIonic } from 'cordova-plugin-fcm-with-dependecy-updated/ionic';
import { Platform } from '@ionic/angular';
import { Device } from '@ionic-native/device/ngx';
import { Network } from '@ionic-native/network/ngx';
import { Storage } from '@ionic/storage';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import { Events } from '@ionic/angular';
import { async } from '@angular/core/testing';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: [
    './styles/login.page.scss'
  ]
})
export class LoginPage implements OnInit {
  userDeviceData = { uuid: "", token: "", platform: "", version: "", member_id: '0' };
  userDeviceResponse: any = { Sucess: '' };

  loginForm: FormGroup;

  validation_messages = {
    'tel': [
      { type: 'required', message: 'Phone is required.' },
      { type: 'pattern', message: 'Enter a valid Phone.' },
      { type: 'minlength', message: 'Phone must be at least 9 characters long.' },
      { type: 'maxlength', message: 'Phone must be at least 10 characters long.' }
    ],
    'password': [
      { type: 'required', message: 'Password is required.' },
      { type: 'minlength', message: 'Password must be at least 5 characters long.' }
    ]
  };

  constructor(
    public router: Router,
    public menu: MenuController,
    public api: WebapiServiceProvider,
    public firebase: AngularFireDatabase,
    public storage: Storage,
    public fcm: FCMPluginOnIonic,
    public platform: Platform,
    public device: Device,
    public network: Network,
    public facebook: Facebook,
    public events: Events
  ) {
    this.loginForm = new FormGroup({
      'tel': new FormControl('', Validators.compose([
        Validators.required, Validators.minLength(9), Validators.maxLength(10),
        Validators.pattern('[0-9]+')
      ])),
      'password': new FormControl('', Validators.compose([
        Validators.minLength(5),
        Validators.required
      ]))
    });
  }

  public lang = 'th'
  ngOnInit(): void {
    this.menu.enable(false);
    this.api.storage_get('lang').then((lang_data) => {
      this.lang = lang_data
      if (this.lang == 'th') {
        this.validation_messages = {
          'tel': [
            { type: 'required', message: 'โปรดกรอกเบอร์โทรศัพท์' },
            { type: 'pattern', message: 'รูปแบบไม่ถูกต้อง' },
            { type: 'minlength', message: 'กรอกอย่างน้อย 9 ตัวอักษร' },
            { type: 'maxlength', message: 'กรอกได้ไม่เกิน 10 ตัวอักษร' }
          ],
          'password': [
            { type: 'required', message: 'โปรดกรอกรหัสผ่าน' },
            { type: 'minlength', message: 'กรอกรหัสผ่านอย่างน้อย 5 ตัวอักษร' }
          ]
        };
      }
    })
  }

  doLogin(): void {
    this.api.postData("login", this.loginForm.value).then(async (res: any) => {
      console.log(res);
      console.log(this.platform.is('mobileweb'));

      if (res.flag == '1') {
        this.api.storage_set("data_login", res.data);
        let tost_login = "ข้อมูลถูกต้อง กำลังเข้าสู่ระบบ"
        if (this.lang == 'en') {
          tost_login = "Login sucess..."
        }
        this.api.Toast(tost_login, 2500, 'bottom');
        if (!this.platform.is('pwa')) {
          setTimeout(() => {
            this.fcm.subscribeToTopic('all');
            this.fcm.getToken().then(token => {

              this.userDeviceData.token = token;
              // Device info
              this.userDeviceData.uuid = this.device.uuid;
              this.userDeviceData.platform = this.device.platform;
              this.userDeviceData.version = this.device.version;
              console.log(this.userDeviceData);

              this.userDeviceData.member_id = res.data.member_id;
              this.api.postData('register_device', this.userDeviceData).then((result: any) => {
                setTimeout(async () => {
                  this.api.checkUserUnknow().then((res) => {
                    let unknow = res
                    if (unknow == 0) {
                      this.router.navigate(['unknow'])
                    } else {
                      this.events.publish('user:login', 'success');
                      this.router.navigate(['/main/tab2/order-wait']);
                    }
                  })
                }, 1000);
                console.log(result + ' ***4*** ');
                this.userDeviceResponse = result;
                if (this.userDeviceResponse.Sucess) {
                  console.log('Regis new device success');
                  //this.api.Toast('Regis new device success', 2500, 'bottom');
                  // Keep UUID to localStorage
                  this.storage.set('uuid', this.device.uuid);
                } else {
                  console.log('Fail! Cannot Regis new device');
                  //this.api.Toast('Fail! Cannot Regis new device', 2500, 'bottom');
                }
              }, (error) => {
                //console.log(error);
                if (error.status == 0) {
                  console.log('Error status Code:0 Web API Offline');
                  //this.api.Toast('Error status Code:0 Web API Offline', 2500, 'bottom');
                }
              });
              // Connect Web API
            }); // then
          }, 600)
        } else {
          this.api.checkUserUnknow().then((res) => {
            let unknow = res
            if (unknow == 0) {
              this.router.navigate(['unknow'])
            } else {
              this.events.publish('user:login', 'success');
              this.router.navigate(['/main/tab2/order-wait']);
            }
          })
        }
      } else {
        let tost_login_false = "เบอร์โทรศัพท์ หรือ รหัสผ่านไม่ถูกต้อง"
        if (this.lang == 'en') {
          tost_login_false = "Invalid numberphone or password."
        }
        this.api.Toast(tost_login_false, 2500, 'bottom');
      }
    })
  }

  goToForgotPassword(): void {
    this.router.navigate(['main/tab2']);
  }

  doFacebookLogin(): void {
    //this.router.navigate(['main/tab2']);
    this.facebook.login(['public_profile', 'email'])
      .then((res: FacebookLoginResponse) => {
        console.log('Logged into Facebook!', res);
        this.getUserDetail(res.authResponse.userID);

      })
      .catch(e => console.log('Error logging into Facebook', e));
  }

  public users = {
    id: '',
    email: '',
    name: '',
    picture: ''
  }
  getUserDetail(userid: any) {
    this.facebook.api('/' + userid + '/?fields=id,email,name,picture', ['public_profile'])
      .then((res: any) => {
        console.log(res);
        let picture = "https://graph.facebook.com/" + res.id + "/picture?width=1024&height=1024";
        this.users = res;
        res.picture = picture;
        this.api.storage_set('fbuser', res)
        this.api.postData('fblogin', { id: res.id, email: res.email, name: res.name, picture: picture }).then((res: any) => {
          let fbstatus: any = res;
          if (res.flag == '1') {
            this.api.storage_set("data_login", res.data);
            if (!this.platform.is('desktop')) {
              this.fcm.subscribeToTopic('all');
              this.fcm.getToken().then(token => {

                this.userDeviceData.token = token;
                // Device info
                this.userDeviceData.uuid = this.device.uuid;
                this.userDeviceData.platform = this.device.platform;
                this.userDeviceData.version = this.device.version;
                console.log(this.userDeviceData);

                this.userDeviceData.member_id = res.data.member_id;
                this.api.postData('register_device', this.userDeviceData).then((result) => {
                  setTimeout(() => {
                    this.router.navigate(['main/tab2']);
                  }, 1000);
                  console.log(result + ' ***5*** ');
                  this.userDeviceResponse = result;
                  if (this.userDeviceResponse.Sucess) {
                    console.log('Regis new device success');
                    //this.api.Toast('Regis new device success', 2000, 'bottom');
                    // Keep UUID to localStorage
                    this.storage.set('uuid', this.device.uuid);
                  } else {
                    console.log('Fail! Cannot Regis new device');
                    //this.api.Toast('Fail! Cannot Regis new device', 2000, 'bottom');
                  }
                }, (error) => {
                  //console.log(error);
                  if (error.status == 0) {
                    console.log('Error status Code:0 Web API Offline');
                    //this.api.Toast('Error status Code:0 Web API Offline', 2000, 'bottom');
                  }
                });
                // Connect Web API
              }); // then
            } else {
              this.router.navigate(['main/tab2']);
            }
          } else {
            // New facebook user login
            this.router.navigate(['auth/confirm-facebook-phone'])
            this.api.Toast('มีผู้ใช้งาน Account นี้เเล้ว', 2000, 'bottom');
          }
        })
      })
      .catch(e => {
        console.log(e);
      });
  }

  doGoogleLogin(): void {
    this.router.navigate(['main/tab2']);
  }

  doTwitterLogin(): void {
    this.router.navigate(['main/tab2']);
  }

  // ngOnDestroy(): void {
  //   this.api.storage_get('data_login').then(res => {
  //     if (res) {
  //       setTimeout(() => {
  //         location.reload()
  //       }, 200);
  //     }
  //   })
  // }

  ionViewWillEnter() {
    console.log('login');
  }
}
