import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { WebapiServiceProvider } from '../providers/webapi-service/webapi-service';
import { Storage } from '@ionic/storage';
import { ModalController, AlertController } from '@ionic/angular';
import { ExchangePage } from './exchange/exchange.page';

@Component({
  selector: 'app-score',
  templateUrl: './score.page.html',
  styleUrls: ['./score.page.scss'],
})
export class ScorePage implements OnInit {

  constructor(
    public route: Router,
    public router: ActivatedRoute,
    public store: Storage,
    public service: WebapiServiceProvider,
    public modalCtr: ModalController,
    public alert: AlertController,
    public ref : ChangeDetectorRef
  ) { }

  public member: any = 0

  ngOnInit() {
    this.member = this.router.snapshot.paramMap.get('member_id')

  }

  public point_total = 0
  public score_detail: any = []
  ionViewDidEnter() {
    this.loadScore()
  }

  loadScore() {
    this.service.getData('score_details/' + this.member).then((data_score: any) => {
      if (data_score != null) {
        this.score_detail = data_score.score
        this.point_total = data_score.total
      }
      this.ref.detectChanges()
    })
  }



  close() {
    this.route.navigate(['../main/account'])
  }

  async exchange(id, point) {
    const modal = await this.modalCtr.create({
      component: ExchangePage,
      componentProps: { member_id: id, point: point },
      mode: 'ios'
    })

    modal.onDidDismiss().then((res) => {
      if (res.data == 'exchanged') {
        this.loadScore()
      }

    })

    await modal.present();
  }

  async doRefresh(event) {
    this.member = await this.router.snapshot.paramMap.get('member_id')
    await this.loadScore()
    setTimeout(() => {
      event.target.complete();
    }, 1500);
  }

}
