import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ScorePage } from './score.page';

const routes: Routes = [
  {
    path: '',
    component: ScorePage
  },  {
    path: 'exchange',
    loadChildren: () => import('./exchange/exchange.module').then( m => m.ExchangePageModule)
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ScorePageRoutingModule {}
