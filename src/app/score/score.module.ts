import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ScorePageRoutingModule } from './score-routing.module';

import { ScorePage } from './score.page';
import { ExchangePage } from './exchange/exchange.page';
import { ExchangePageModule } from './exchange/exchange.module';
import { NgxIonicImageViewerModule } from 'ngx-ionic-image-viewer';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ScorePageRoutingModule,
    ExchangePageModule,
    TranslateModule.forChild(),
    NgxIonicImageViewerModule
  ],
  declarations: [ScorePage],
  entryComponents : [ExchangePage]
})
export class ScorePageModule {}
