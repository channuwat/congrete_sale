import { Component, OnInit, Input } from '@angular/core';
import { Storage } from '@ionic/storage';
import { WebapiServiceProvider } from 'src/app/providers/webapi-service/webapi-service';
import { ModalController, AlertController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-exchange',
  templateUrl: './exchange.page.html',
  styleUrls: ['./exchange.page.scss'],
})
export class ExchangePage implements OnInit {
  @Input() member_id: number
  @Input() point: number
  constructor(
    public strore: Storage,
    public api: WebapiServiceProvider,
    public modalCtr: ModalController,
    public alert: AlertController,
    public route: Router
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.getBankExchangePoint(this.member_id)
  }

  public bankes: any = []
  public bank_select = 0
  getBankExchangePoint(member_id) {
    this.api.getData('getBankExchangePoint/' + member_id).then((res: any) => {
      console.log(res);

      if (res.length > 0) {
        this.bankes = res
        this.bank_select = res[0].bb_id
        console.log(res);
      }
    })
  }

  async exchangeConfirm() {
    var points = this.point
    var message = 'ยืนยันการแลกคะแนน'
    var btn = [
      {
        text: 'ยกเลิก',
        role: 'cancel',
        cssClass: 'secondary',
        handler: (blah) => {
        }
      }, {
        text: 'แลกคะแนน',
        handler: () => {
          var data = {
            member_id: this.member_id,
            bank: this.bank_select,
            point: points
          }
          
          this.api.postData('exchangePoint', data).then((res: any) => {
            setTimeout(() => {
              this.point = 0
              this.api.fb_set('alert/menu/score')
              this.modalCtr.dismiss('exchanged')
            }, 500);

          })
        }
      }
    ]
    if (points < 1) {
      message = 'คะแนนสะสมไม่เพียงพอ'
      btn = [
        {
          text: 'ฉันเข้าใจ',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }
      ]
    }
    const alert = await this.alert.create({
      cssClass: 'my-custom-class',
      message: message,
      buttons: btn
    });

    await alert.present();
  }

  doRefresh(event) {
    this.getBankExchangePoint(this.member_id)
    setTimeout(() => {
      event.target.complete();
    }, 1000);
  }

  addBookBank(uri) {
    this.route.navigate([uri])
    setTimeout(() => {
      this.close()
    }, 500);
  }

  close() {
    this.modalCtr.dismiss()
  }

}
