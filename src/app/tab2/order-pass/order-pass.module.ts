import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrderPassPageRoutingModule } from './order-pass-routing.module';

import { OrderPassPage } from './order-pass.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrderPassPageRoutingModule,
    TranslateModule.forChild(),
  ],
  declarations: [OrderPassPage]
})
export class OrderPassPageModule {}
