import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrderPassPage } from './order-pass.page';

const routes: Routes = [
  {
    path: '',
    component: OrderPassPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrderPassPageRoutingModule {}
