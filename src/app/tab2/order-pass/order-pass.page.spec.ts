import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OrderPassPage } from './order-pass.page';

describe('OrderPassPage', () => {
  let component: OrderPassPage;
  let fixture: ComponentFixture<OrderPassPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderPassPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OrderPassPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
