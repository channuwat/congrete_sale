import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { WebapiServiceProvider } from 'src/app/providers/webapi-service/webapi-service';
import { ModalController } from '@ionic/angular';
import { AuctionPage } from 'src/app/auction/auction.page';
import { setInterval } from 'core-js';
import { async } from 'rxjs/internal/scheduler/async';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'app-order-wait',
  templateUrl: './order-wait.page.html',
  styleUrls: ['./order-wait.page.scss'],
})
export class OrderWaitPage implements OnInit {
  public data: any = { member_id: '' };
  public new_order = [];
  constructor(
    public api: WebapiServiceProvider,
    public modalController: ModalController,
    private ref: ChangeDetectorRef,
    public fb: AngularFireDatabase
  ) { }

  public search = 5
  ngOnInit() {
    this.api.fb_val('order', () => {
      this.api.storage_get('data_login').then((data: any) => {
        if (data) {
          this.data = data;
          setTimeout(() => {
            this.loadList(this.search)
          }, 500);
        }
      });
    })
  }

  public lang = 'th'
  public time_ViewDidEnter = 0
  ionViewDidEnter() {
    this.api.storage_get('lang').then((lang_res) => {
      if (lang_res == 'en') {
        this.lang = 'en'
      }
    })
    setInterval(() => {
      this.time_ViewDidEnter++;
    }, 1000)
  }

  loadList(count) {
    this.api.getData('order_data_out/0/' + this.data.member_id + '/' + count + '/data').then(async (res: any) => {
      this.new_order = await res;
      //this.fb.database.ref('count_group_order/'+this.data.member_id+'/wait').set(this.new_order.length)
      this.ref.detectChanges();
    });
  }

  searchList(count) {
    if (count < 5) {
      this.search = 5
    } else {
      this.search = 0
    }
    this.loadList(this.search)
  }

  async select(id, type) {
    const modal = await this.modalController.create({
      component: AuctionPage,
      componentProps: {
        order_data: { order_id: id, auct_type: type }
      },
      // mode: 'ios'
    });
    modal.onDidDismiss().then((res: any) => {
      console.log(res.data);
    });
    return await modal.present();
  }

  async refresh(event) {
    await this.loadList(0)
    await setTimeout(() => {
      event.target.complete();
    }, 500);
  }
}