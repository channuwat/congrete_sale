import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrderWaitPageRoutingModule } from './order-wait-routing.module';

import { OrderWaitPage } from './order-wait.page';
import { ComponentsModule } from 'src/app/components/components.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    TranslateModule.forChild(),
    OrderWaitPageRoutingModule
  ],
  declarations: [OrderWaitPage]
})
export class OrderWaitPageModule {}
