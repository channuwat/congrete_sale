import { Component, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { WebapiServiceProvider } from '../providers/webapi-service/webapi-service';
import { async } from '@angular/core/testing';
import { AlertController, Events, LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  public data: any = { member_id: '', picture: '', name: '' }
  public activePage = this.router.url
  constructor(
    public router: Router,
    public api: WebapiServiceProvider,
    public routeUri: ActivatedRoute,
    public ref: ChangeDetectorRef,
    public alertController: AlertController,
    public loadingController: LoadingController,
    public events: Events
  ) {
    this.events.subscribe('user:login', async (res) => {
      if (res == 'success') {
        console.log(res);
        await this.ionViewWillEnter();
        await this.ionViewDidEnter();
      }
    });

    this.api.storage_get('data_login').then(async (data: any) => {
      if (data) {
        this.data = await data
        await this.api.getData('getLocation/' + data.member_id).then(async (local: number) => {
          if (local < 1 && data.approve == 1) {
            let alertStr = {
              th: { head: 'ไม่พบสถานที่', body: 'โปรดระบุสถานที่ให้บริการของท่าน<br>ไปที่ "บัญชี->ตั้งค่าพื้นที่ให้บริการ"', btn: 'ตั้งค่า' },
              en: { head: 'ไม่พบสถานที่ EN', body: 'โปรดระบุสถานที่ให้บริการของท่าน EN', btn: 'ตั้งค่า EN' }
            }
            const alert = await this.alertController.create({
              header: alertStr.th.head,
              message: alertStr.th.body,
              buttons: [
                {
                  text: alertStr.th.btn,
                  handler: () => {
                    this.router.navigate(['main/account/setting-location-order', { page: 'home' }])
                  }
                }
              ]
            });

            await alert.present();
          }
        })
        //await this.countGroup(data.member_id)
      }

    })


  }

  public state = false
  async ionViewWillEnter() {
    await this.api.storage_get('data_login').then(async (data: any) => {
      if (data) {
        this.data = await data
      }
    })

    this.state = await true
    await setTimeout(() => {
      this.api.fb_val('order', () => {
        if (this.state) {
          if (this.data.member_id != '') {
            this.api.getData('countGroup/0/' + this.data.member_id + '/' + 0 + '/count').then(async (c: any) => {
              this.count.wait = c.wait
              this.count.wait_send = c.wait_send
              this.count.send = c.order_send
              this.count.pass = c.pass
              // this.count.success = c.success
              this.ref.detectChanges()
            })
          }

        }
      })
    }, 800);
  }

  public count: any = []
  async ionViewDidEnter() {
    // กำหนด uri ลง Store //
    this.api.lastLocationURI('set')
    this.activePage = this.router.url
    this.api.checkUserUnknow().then((res) => {
      let unknow = res
      if (unknow == 0) {
        this.router.navigate(['unknow'])
      }
    })
  }

  ionViewDidLeave() {
    this.state = false
  }

  async refresh(event) {
    event.target.complete();
    const loading = await this.loadingController.create({
      message: 'กรุณารอสักครู่...',
      duration: 1500
    });
    await loading.present();
    location.reload()
  }

  page(uri) {
    this.router.navigate(['/main/tab2/' + uri]);
  }
  setting() {
    this.router.navigate(['main/account/setting-location-order', { page: 'home' }]);
  }

}
