import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { IonInfiniteScroll, IonContent, ModalController, AlertController } from '@ionic/angular';
import { WebapiServiceProvider } from 'src/app/providers/webapi-service/webapi-service';
import { AuctionPage } from 'src/app/auction/auction.page';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'app-order-success',
  templateUrl: './order-success.page.html',
  styleUrls: ['./order-success.page.scss'],
})
export class OrderSuccessPage implements OnInit {
  @ViewChild(IonInfiniteScroll, { static: false }) infiniteScroll: IonInfiniteScroll;
  @ViewChild(IonContent, { static: false }) content: IonContent;
  public data: any = { member_id: '' };
  public new_order: any = [];
  constructor(
    public api: WebapiServiceProvider,
    public modalController: ModalController,
    private ref: ChangeDetectorRef,
    public alert: AlertController,
    public fb: AngularFireDatabase
  ) { }

  public search = 0
  ngOnInit() {
    this.api.fb_val('order', () => {
      this.api.storage_get('data_login').then((data: any) => {
        this.data = data;
        this.loadList(this.search)
      });
    })
  }

  public last_length = 0
  loadData(event) {
    setTimeout(() => {
      event.target.complete();
      this.search += 10
      this.api.getData('order_data_final/3/' + this.data.member_id + '/' + this.search + '/data').then((res: any) => {
        //this.new_order = res;
        res.forEach(val => {
          this.new_order.push(val);
        });
        console.log(this.new_order);

        // this.fb.database.ref('count_group_order/' + this.data.member_id + '/success').set(this.new_order.length)
        this.ref.detectChanges();
      });
    }, 500);
  }


  loadList(count) {
    this.api.getData('order_data_final/3/' + this.data.member_id + '/' + count + '/data').then((res: any) => {
      this.new_order = res;
      console.log(this.new_order);

      // this.fb.database.ref('count_group_order/' + this.data.member_id + '/success').set(this.new_order.length)
      this.ref.detectChanges();
    });
  }

  searchList(count) {
    if (count > 10) {
      this.search = 10
    }
    this.loadList(this.search)
  }

  async select(id, type) {
    const modal = await this.modalController.create({
      component: AuctionPage,
      componentProps: {
        order_data: { order_id: id, auct_type: type },
        // mode: 'ios'
      }
    });
    modal.onDidDismiss().then((res: any) => {
      console.log(res.data);
    });
    return await modal.present();
  }

  async Swipe(ap_id) {
    const alert = await this.alert.create({
      cssClass: 'my-custom-class',
      header: 'ลบการแจ้งเตือนนี้',
      buttons: [{
        text: 'ลบ',
        handler: () => {
          this.api.getData('hideOrderSuccess/' + ap_id).then((res) => {
            this.search = 0
            this.loadList(this.search)
            //this.api.fb_set('order')
          })
        }
      }
      ]
    });

    await alert.present();
  }

  async refresh(event) {
    await this.loadList(0)
    await setTimeout(() => {
      event.target.complete();
    }, 500);
  }
}
