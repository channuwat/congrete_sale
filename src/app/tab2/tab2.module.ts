import { OrderPageRoutingModule } from './order-routing.module';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Tab2Page } from './tab2.page';
import { TranslateModule } from '@ngx-translate/core';
//import { VerifyComponentModule } from '../verify-component/verify-component.module';
import { AuctionSlipComponent } from '../auction/auction-slip/auction-slip.component';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    OrderPageRoutingModule,
    TranslateModule.forChild(),
    RouterModule.forChild([{ path: '', component: Tab2Page }]),
    //VerifyComponentModule
  ],
  declarations: [Tab2Page],
})
export class Tab2PageModule { }
