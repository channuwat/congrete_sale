import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Tab2Page } from './tab2.page';
const routes: Routes = [
  {
    path: '',
    component: Tab2Page,
    children:
      [
        {
          path: 'order-wait',
          loadChildren: () => import('./order-wait/order-wait.module').then(m => m.OrderWaitPageModule)
        },
        {
          path: 'order-wait-send',
          loadChildren: () => import('./order-wait-send/order-wait-send.module').then(m => m.OrderWaitSendPageModule)
        },
        {
          path: 'order-send',
          loadChildren: () => import('./order-send/order-send.module').then(m => m.OrderSendPageModule)
        },
        {
          path: 'order-pass',
          loadChildren: () => import('./order-pass/order-pass.module').then(m => m.OrderPassPageModule)
        },
        {
          path: 'order-success',
          loadChildren: () => import('./order-success/order-success.module').then(m => m.OrderSuccessPageModule)
        },
        {
          path: '',
          redirectTo: 'order-wait',
          pathMatch: 'full'
        }
      ]
  },
  {
    path: 'unknow',
    loadChildren: () => import('../verify-component/verify-component.module').then(m => m.VerifyComponentModule)
  },
  {
    path: 'tab2/order-wait',
    redirectTo: '',
    pathMatch: 'full'
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrderPageRoutingModule { }
