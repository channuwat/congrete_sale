import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrderWaitSendPageRoutingModule } from './order-wait-send-routing.module';

import { OrderWaitSendPage } from './order-wait-send.page';
import { ComponentsModule } from 'src/app/components/components.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    OrderWaitSendPageRoutingModule,
    TranslateModule.forChild()
  ],
  declarations: [OrderWaitSendPage]
})
export class OrderWaitSendPageModule {}
