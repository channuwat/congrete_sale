import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { WebapiServiceProvider } from 'src/app/providers/webapi-service/webapi-service';
import { ModalController, IonInfiniteScroll, IonContent } from '@ionic/angular';
import { AuctionPage } from 'src/app/auction/auction.page';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'app-order-wait-send',
  templateUrl: './order-wait-send.page.html',
  styleUrls: ['./order-wait-send.page.scss'],
})
export class OrderWaitSendPage implements OnInit {
  @ViewChild(IonInfiniteScroll, { static: false }) infiniteScroll: IonInfiniteScroll;
  @ViewChild(IonContent, { static: false }) content: IonContent;
  public data: any = { member_id: '' };
  public new_order = [];
  constructor(
    public api: WebapiServiceProvider,
    public modalController: ModalController,
    private ref: ChangeDetectorRef,
    public fb: AngularFireDatabase
  ) { }

  public search = 5
  ngOnInit() {
    this.api.fb_val('order', () => {
      this.api.storage_get('data_login').then((data: any) => {
        this.data = data;
        this.loadList(this.search)
      });
    })
  }

  public time_ViewDidEnter = 0
  ionViewDidEnter() {
    setInterval(() => {
      this.time_ViewDidEnter++;
    }, 1000)
  }

  public last_length = 0
  loadData(event) {
    console.log(this.last_length, this.new_order.length);

    if (this.last_length != this.new_order.length) {
      setTimeout(() => {
        event.target.complete();
        this.search += 5
        this.loadList(this.search)
        this.last_length = this.new_order.length
      }, 500);
    } else {
      this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
    }
  }

  loadList(count) {
    this.api.getData('order_data_in/0/' + this.data.member_id + '/' + count + '/data').then((res: any) => {
      this.new_order = res;
      //this.fb.database.ref('count_group_order/'+this.data.member_id+'/wait_send').set(this.new_order.length)
      this.ref.detectChanges();
    });
  }

  searchList(count) {
    if (count < 5) {
      this.search = 5
    } else {
      this.search = 0
    }
    this.loadList(this.search)
  }

  async select(id, type) {
    const modal = await this.modalController.create({
      component: AuctionPage,
      componentProps: {
        order_data: { order_id: id, auct_type: type }
      },
      // mode: 'ios'
    });
    modal.onDidDismiss().then((res: any) => {
      console.log(res.data);
    });
    return await modal.present();
  }

  async refresh(event) {
    await this.loadList(0)
    await setTimeout(() => {
      event.target.complete();
    }, 500);
  }

}
