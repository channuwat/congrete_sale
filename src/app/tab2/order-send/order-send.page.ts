import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { WebapiServiceProvider } from 'src/app/providers/webapi-service/webapi-service';
import { ModalController, IonInfiniteScroll, IonContent } from '@ionic/angular';
import { AuctionPage } from 'src/app/auction/auction.page';
import { async } from '@angular/core/testing';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'app-order-send',
  templateUrl: './order-send.page.html',
  styleUrls: ['./order-send.page.scss'],
})
export class OrderSendPage implements OnInit {
  @ViewChild(IonInfiniteScroll, { static: false }) infiniteScroll: IonInfiniteScroll;
  @ViewChild(IonContent, { static: false }) content: IonContent;
  public data: any = { member_id: '' };
  public new_order = [];
  constructor(
    public api: WebapiServiceProvider,
    public modalController: ModalController,
    private ref: ChangeDetectorRef,
    public fb: AngularFireDatabase
  ) { }

  public search = 10
  ngOnInit() {
    this.api.fb_val('order', () => {
      this.api.storage_get('data_login').then((data: any) => {
        this.data = data;
        this.loadList(this.search)
      });
    })
  }

  public lang = 'th'
  ionViewDidEnter() {
    this.api.storage_get('lang').then((lang_res)=>{
      if(lang_res == 'en'){
        this.lang = 'en'
      }
    })
  }

  public last_length = 0
  loadData(event) {
    if (this.last_length != this.new_order.length) {
      setTimeout(() => {
        event.target.complete();
        this.search += 10
        this.loadList(this.search)
        this.last_length = this.new_order.length
      }, 1500);
    } else {
      this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
    }
  }


  loadList(count) {
    this.api.getData('order_data_pay/0/' + this.data.member_id + '/' + count + '/data').then(async (res: any) => {
      this.new_order = await res;
      //this.fb.database.ref('count_group_order/'+this.data.member_id+'/send').set(this.new_order.length)
      this.ref.detectChanges();
    });
  }

  searchList(count) {
    if (count > 10) {
      this.search = 10
    }
    this.loadList(this.search)
  }

  async select(id, type) {
    const modal = await this.modalController.create({
      component: AuctionPage,
      componentProps: {
        order_data: { order_id: id, auct_type: type }
      },
      //mode: 'ios'
    });
    modal.onDidDismiss().then((res: any) => {
      console.log(res.data);
    });
    return await modal.present();
  }

  async refresh(event) {
    await this.loadList(0)
    await setTimeout(() => {
      event.target.complete();
    }, 500);
  }

}
