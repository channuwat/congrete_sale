import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrderSendPageRoutingModule } from './order-send-routing.module';

import { OrderSendPage } from './order-send.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule.forChild(),
    OrderSendPageRoutingModule
  ],
  declarations: [OrderSendPage]
})
export class OrderSendPageModule {}
