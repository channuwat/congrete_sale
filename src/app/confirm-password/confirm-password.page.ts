import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { WebapiServiceProvider } from '../providers/webapi-service/webapi-service';
import { Storage } from '@ionic/storage';
import { async } from '@angular/core/testing';

@Component({
  selector: 'app-confirm-password',
  templateUrl: './confirm-password.page.html',
  styleUrls: ['./confirm-password.page.scss'],
})
export class ConfirmPasswordPage implements OnInit {
  signupForm: FormGroup;
  matching_passwords_group: FormGroup;


  validation_messages = {
    "tel": [
      { type: 'required', message: 'Phone is required.' },
      { type: 'pattern', message: 'Enter a valid Phone.' },
      { type: 'minlength', message: 'Phone must be at least 9 characters long.' },
      { type: 'maxlength', message: 'Phone must be at least 10 characters long.' }
    ],

  };
  public code = '';
  public confirm: string = ''
  constructor(
    public router: Router,
    public api: WebapiServiceProvider,
    public route: ActivatedRoute,
    public store: Storage
  ) {
    this.confirm = this.route.snapshot.paramMap.get('confirm')
    api.storage_get('otp_code').then((code) => {
      this.code = code;
    });
    this.signupForm = new FormGroup({
      otp1: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[0-9]+')])),
      otp2: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[0-9]+')])),
      otp3: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[0-9]+')])),
      otp4: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[0-9]+')])),
      otp5: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[0-9]+')])),
      otp6: new FormControl('', Validators.compose([Validators.required, Validators.pattern('[0-9]+')])),
    });
  }
  async doSignup() {
    console.log(this.signupForm.value);
    this.api.storage_get('otp_phone').then((val: any) => {
      let otp = this.signupForm.value.otp1 + this.signupForm.value.otp2 + this.signupForm.value.otp3 + this.signupForm.value.otp4 + this.signupForm.value.otp5 + this.signupForm.value.otp6
      console.log(otp);
      
      this.api.postData('confirm_otp', { otp: otp, tel: val }).then((result: any) => {
        console.log(result);
        if (result.flag == '1') {
          if (this.confirm == 'forgot') {
            this.router.navigate(['/auth/add-password/login']);
          } else if (this.confirm == 'facebook') {
            this.store.get('fbuser').then((fb_data: any) => {
              this.store.get('phone_confirm').then((phone: any) => {
                var data = {
                  phone: phone,
                  fb_id: fb_data.id,
                  email: fb_data.email,
                  name: fb_data.name,
                  picture: fb_data.picture,
                }
                this.api.postData('registerFacebook', data).then((last_insert) => {
                  this.api.storage_set("data_login", last_insert);
                }).then(() => {
                  this.router.navigate(['main/home']);
                })
              })
            })

          } else if (this.confirm == 'facebook-phone') {
            this.store.get('fbuser').then((fb_data: any) => {
              this.store.get('phone_confirm').then((phone: any) => {
                var data = {
                  phone: phone,
                  fb_id: fb_data.id,
                }
                this.api.postData('updateFacebook', data).then((last_update) => {
                  this.api.storage_set("data_login", last_update);
                }).then(() => {
                  this.router.navigate(['main/home']);
                })
              })
            })
          } else {
            this.router.navigate(['/auth/change-password']);
          }

        } else {
          this.api.Toast("รหัสไม่ถูกต้อง กรุณาลองใหม่อีกครั้ง หากยังไม่ได้รับให้กดส่งรหัสอีกครั้ง", 3000, 'top')
        }
      })
    });


  }
  ngOnInit() {
  }
  send_agen() {
    this.api.storage_get('otp_phone').then((val: any) => {
      this.api.postData('add_otp', { tel: val, type: 2 }).then((res: any) => {
        console.log(res);
        if (res.flag == '0') {
          this.api.Toast(res.error, 2500, 'top');
        } else {
          this.api.Toast("ระบบได้ส่ง SMS ไปยังเบอร์ของคุณเรียบร้อยแล้ว", 3000, 'top');

        }
      });
    });
  }

  otpController(event, next, prev) {
    if (event.target.value.length < 1 && prev) {
      prev.setFocus()
    }
    else if (next && event.target.value.length > 0) {
      next.setFocus();
    }
    else {
      return 0;
    }
  }
}
