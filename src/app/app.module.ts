
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { ComponentsModule } from './components/components.module';

import { ServiceWorkerModule } from '@angular/service-worker';

import { environment } from '../environments/environment';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { NotificationDetailPage } from './notification-detail/notification-detail.page';
import { IonicStorageModule } from '@ionic/storage';
import { WebapiServiceProvider } from './providers/webapi-service/webapi-service';
import { IonicSelectableModule } from 'ionic-selectable';
import { AuctionPage } from './auction/auction.page';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { FCMPluginOnIonic } from 'cordova-plugin-fcm-with-dependecy-updated/ionic';
import { Device } from '@ionic-native/device/ngx';
import { Network } from '@ionic-native/network/ngx';
import { Facebook } from '@ionic-native/facebook/ngx';
import { TermsOfServicePage } from './terms-of-service/terms-of-service.page';
import { PrivacyPolicyPage } from './privacy-policy/privacy-policy.page';

import { MobileAccessibility } from '@ionic-native/mobile-accessibility/ngx';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';


export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [AppComponent, AuctionPage,TermsOfServicePage,PrivacyPolicyPage],
  entryComponents: [AuctionPage],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireDatabaseModule,
    IonicSelectableModule,
    AppRoutingModule,
    IonicStorageModule.forRoot(),
    ComponentsModule,
    //VerifyComponentModule,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production }),
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    })

  ],
  providers: [
    FCMPluginOnIonic,Device,Network,YoutubeVideoPlayer,
    WebapiServiceProvider,SplashScreen,
    Facebook,MobileAccessibility,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
