import { Component, OnInit, ChangeDetectorRef, Input } from '@angular/core';
import { WebapiServiceProvider } from 'src/app/providers/webapi-service/webapi-service';
import { AlertController } from '@ionic/angular';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'app-auction-slip',
  templateUrl: './auction-slip.component.html',
  styleUrls: ['./auction-slip.component.scss'],
})
export class AuctionSlipComponent implements OnInit {

  @Input() order_data: any;
  constructor(
    public service: WebapiServiceProvider,
    public alert: AlertController,
    public ref: ChangeDetectorRef,
    public fb: AngularFireDatabase
  ) { }

  public sale: any = []
  ngOnInit() {
    this.service.storage_get('data_login').then(async (data_sale) => {
      this.sale = data_sale
      setTimeout(() => {
        this.loadDataOrder()
      }, 200);

    })
  }

  public slip: any = {
    order: {
      id: null,
      send_date: null
    },
    items:
      [
        { item: { en: null, th: null }, val: null, qty: null, unit: { en: null, th: null } }
      ]
    ,
    total: null,
    location: {
      customer: {
        name: null,
        address: null,
        phone: null,
      },
      sale: {
        name: null,
        email: null,
        phone: null,
        address: null,
      }
    }
  }
  public lang = ''
  loadDataOrder() {
    this.service.getData('winAuction/' + this.order_data.ap_id).then(async (res: any) => {
      var sale: any = {}
      await this.service.storage_get('data_login').then(user => {
        sale = user
      })

      this.service.storage_get('lang').then(lang => {
        this.lang = lang
      })

      let roundPass = 1
      if (this.order_data.car == 1) {
        roundPass = Math.ceil(this.order_data.count / 2)
      } else {
        roundPass = Math.ceil(this.order_data.count / 5)
      }

      let order: any = await {
        id: this.order_data.order_id,
        send_date: this.order_data.send_date + ' / ' + this.order_data.send_time.substring(0, 5)
      }

      let sendChk = { item: { en: 'shipping (free)', th: 'ค่าส่ง (ฟรี)' }, val: res.ap_diff_price, qty: roundPass, unit: { en: 'round', th: 'เที่ยว' } }
      if (this.order_data.count < 3) {
        sendChk = { item: { en: 'shipping', th: 'ค่าส่ง' }, val: res.ap_diff_price, qty: roundPass, unit: { en: 'round', th: 'เที่ยว' } }
      }
      let items = await [
        { item: { en: this.order_data.p_name.en, th: this.order_data.p_name.th }, val: res.ap_price, qty: Number(this.order_data.count), unit: { en: 'cube', th: 'คิว' } },
        sendChk
      ]

      let total = 0
      items.forEach(item => {
        total += item.val * item.qty
      });
      // console.log(res);

      let location = await {
        customer: {
          name: this.order_data.name,
          address: this.order_data.address + ' ต.' + this.order_data.subdistrict + ' อ.' + this.order_data.district + ' จ.' + this.order_data.province,
          phone: this.order_data.phone
        },
        sale: {
          name: sale.name,
          email: sale.email,
          phone: sale.phone,
          address: sale.address,
        }
      }

      this.slip = await {
        order: order,
        items: items,
        total: total,
        location: location
      }

      // set date format //
      var day = this.slip.order.send_date.substring(0, 4)
      var mount = this.slip.order.send_date.substring(5, 7)
      var year = this.slip.order.send_date.substring(8, 11)
      var time = this.slip.order.send_date.substring(13, 18)
      this.slip.order.send_date = day + '/' + mount + '/' + year + ' | ' + time
    })
  }

}
