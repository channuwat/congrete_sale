import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { ModalController, NavParams, AlertController } from '@ionic/angular';
import { WebapiServiceProvider } from '../providers/webapi-service/webapi-service';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { AngularFireDatabase } from '@angular/fire/database';
import { async } from 'rxjs/internal/scheduler/async';

@Component({
  selector: 'app-auction',
  templateUrl: './auction.page.html',
  styleUrls: ['./auction.page.scss'],
})
export class AuctionPage implements OnInit {
  public auctForm: FormGroup
  @Input() order_data: any
  public data = {
    order_id: '',
    p_name: '',
    send_date: '',
    send_time: "",
    receive_name: '',
    count: '',
    car: '',
    address: '',

  };
  constructor(
    public modal: ModalController,
    public NavParams: NavParams,
    public service: WebapiServiceProvider,
    public alert: AlertController,
    public ref: ChangeDetectorRef,
    public fb: AngularFireDatabase
  ) {
    this.auctForm = new FormGroup({
      'auct_price': new FormControl(1, [Validators.required, Validators.min(1)]),
      'around_price': new FormControl(500, [Validators.required])
    });
  }

  public sale: any = []
  ngOnInit() {
    this.statePage = true
    this.data = this.NavParams.get("data");
  }


  ionViewWillEnter() {
    this.service.fb_val('order', () => {
      if (this.statePage) {
        this.service.storage_get('data_login').then(async (data_sale) => {
          this.sale = await data_sale
          await this.loadDataOrder()
        })
      }
    })
  }

  public auct_type: string = ''
  public round_price: any = 1
  ionViewDidEnter() {
    this.auct_type = this.order_data.auct_type
    if (this.order_head.car == 1) {
      this.round_price = Math.ceil(this.order_head.count / 2)
    } else {
      this.round_price = Math.ceil(this.order_head.count / 5)
    }

  }

  public statePage = false
  public order_head: any = {
    p_name: { th: '' }
    , format: { type: 0 }
  }
  loadDataOrder() {
    console.log(this.order_data);

    this.service.getData('auctionOrder/' + this.order_data.order_id + '/' + this.sale.member_id).then((data_res: any) => {
      this.order_head = data_res;
      if (this.order_head.format.type == 1) {
        this.order_head.order_formatSTR = {
          th: 'เสนอราคาได้ครั้งเดียว',
          en: 'One bids'
        }
      } else {
        this.order_head.order_formatSTR = {
          th: 'เสนอราคาได้หลายครั้ง',
          en: 'Multiple bids'
        }
      }
      console.log(this.order_head);

      this.loadDetailOrderPassing()
    })
    this.loadListDataOrder()
  }

  public order_auct: any = []
  public order_auct_min: any = []
  public order_auct_win: any = []
  loadListDataOrder() {
    this.service.getData('auctionListOrder/' + this.order_data.order_id).then((data_res: any) => {
      this.order_auct = data_res.list
      this.order_auct_min = data_res.row_min
      this.order_auct_win = data_res.row_win
      if (this.order_auct_min != '') {
        setTimeout(() => {
          this.price_last = this.order_auct_min.ap_price
          this.auctForm.get('around_price').setValue(500)
          //this.price_cube = this.price_last * this.order_head.count
          this.ref.detectChanges()
        }, 500);

      }

      this.ref.detectChanges()
    })
  }

  public price_last = 0
  public around_price_last = 500
  changePrice(type, val, now_price) {
    if (type == 'add') {
      this.price_last = Number(now_price + val)
      setTimeout(() => {
        this.calCube(this.price_last)
      }, 500);
    } else {
      this.price_last = Number(now_price - val)
      if (this.price_last < 0) {
        this.service.Toast('ราคาประมูลต่ำกว่ากำหนด', 4000, 'bottom')
        this.price_last = Number(now_price + val)
      }
      setTimeout(() => {
        this.calCube(this.price_last)
      }, 500);
    }
  }

  public price_cube = 0
  calCube(price) {
    if (price != 0) {
      var around_price = this.auctForm.value.around_price
      if (this.order_head.count >= 3) {
        around_price = 0
      }
      this.price_last = Number(price)
      this.price_cube = Number((price * this.order_head.count))
      if (this.round_price < 3) {
        this.price_cube = Math.ceil(this.price_cube + Number((around_price * this.round_price)))
      }
    }
  }

  lessThanAuction(order_id, price_cube) {
    // set auction less than order
    // let min = 0
    // let defult = 0
    // let round = 0
    // let status_insert = false


    // this.order_auct.forEach(el => {
    //   if (this.order_head.car = 1) {
    //     round = Math.ceil(this.order_head.count / 2)
    //   } else {
    //     round = Math.ceil(this.order_head.count / 5)
    //   }
    //   defult = (el.ap_price * this.order_head.count) + (el.ap_diff_price * round)
    //   // หาค่าประมูลที่น้อยที่สุดจากรายการทั้งหมด
    //   if (min == 0) {
    //     min = (el.ap_price * this.order_head.count) + (el.ap_diff_price * round)
    //   } else {
    //     if (min > defult) {
    //       min = defult
    //     } else {
    //     }
    //   }
    // });


    // min = (this.order_auct_min.ap_price * this.order_head.count) + (this.round_price * this.order_auct_min.ap_diff_price)
    // if (this.order_head.count >= 3) {
    //   min = this.order_auct_min.ap_price * this.order_head.count
    // }
    // // รายการที่เพิ่มมาใหม่ต้องน้อยกว่าตอนเเรก
    // if (this.price_cube < min) {
    //   status_insert = true
    // }
    var data = {
      order_id: order_id,
      price_cube: price_cube
    }
    let auction = false
    this.service.postData('checkLowAuction', data).then((low_data: any) => {
      auction = low_data
      return auction
    })
  }

  async auctionOrder() {
    // set data insert
    var around_price = this.auctForm.value.around_price
    if (this.order_head.count >= 3) {
      around_price = 0
    }
    let data = await {
      order_id: this.order_head.order_id,
      customer_id: this.order_head.member_id,
      sale_id: this.sale.member_id,
      ap_price: Math.floor(this.price_last),
      ap_diff_price: around_price,
      round: this.round_price,
      count: this.order_head.count,
      rate: this.order_head.my_rate,
    }



    // set confirm alert before insert
    let header = await 'ยืนยันราคาประมูล'
    let ms = await '<p>จำนวนเงิน : ' + this.price_last + ' บาท</p>'
    ms = await ms + '<p>ค่าเที่ยว/รอบ : ' + data.ap_diff_price + ' บาท</p>'
    ms = await ms + '<p>จำนวน : ' + this.round_price + ' รอบ</p>'
    ms = await ms + '<p><span style="border-bottom: double;">รวมทั้งหมด : ' + this.price_cube + ' บาท</span></p>'
    let btn_cancel = await 'ยกเลิก'
    let btn_confirm = await 'ยืนยัน'
    let tost_no_price = await 'โปรดระบุราคาค่าจัดส่ง'
    let tost_more_price = await 'โปรดระบุราคาให้ต่ำกว่าราคาปัจจุบัน...'
    if (this.lang == 'en') {
      header = 'Confirm'
      ms = '<p>Price : ' + this.price_last + ' Bath</p>'
      ms = ms + '<p>Cost/Round : ' + data.ap_diff_price + ' Bath</p>'
      ms = ms + '<p>Unit : ' + this.round_price + ' Round</p>'
      ms = ms + '<p><span style="border-bottom: double;">Total : ' + this.price_cube + ' Bath</span></p>'
      btn_cancel = 'cancel'
      btn_confirm = 'confirm'
      tost_no_price = 'Please specify the price'
      tost_more_price = 'Please fill the price is lower than the current price...'
    }
    // create alert
    var alert = await this.alert.create({
      header: header,
      message: ms,
      buttons: [
        {
          text: btn_cancel,
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            //----- some code --------//
          }
        }, {
          text: btn_confirm,
          handler: () => {
            var insert = false
            var data_check = {
              order_id: this.order_head.order_id,
              price_cube: this.price_cube
            }
            let status_insert = false
            this.service.postData('checkLowAuction', data_check).then((low_data: any) => {
              status_insert = low_data
              if (status_insert) {
                if (this.order_head.count < 3) {
                  if (data.ap_diff_price < 1) {
                    this.service.Toast(tost_no_price, 4000, 'bottom')
                  } else {
                    insert = true
                  }
                } else {
                  insert = true
                }

                if (insert == true) {
                  this.service.postData('addAuctionOrder', data).then((array: any) => {
                    if (array.error == 'success') {
                      this.service.fb_set('order')
                      this.loadDataOrder()
                      this.service.Toast('สำเร็จ√', 2500, 'bottom')
                      array.in_order.forEach(el => {
                        if (el.sale_id != this.sale.member_id) {
                          this.fb.database.ref('notification/user_' + el.sale_id + '/alert').set(1);
                        }

                      });
                      this.fb.database.ref('notification/user_' + this.order_head.member_id + '/alert').set(1);
                      this.sentNotificationAuct(data)
                    } else {
                      this.falseAuction()
                    }
                  })
                }
              } else {
                this.service.Toast(tost_more_price, 4000, 'bottom')
              }
            })
          }
        }
      ],
      mode: "ios"
    });
    await alert.present();
  }

  async falseAuction() {
    const fail_auction = await this.alert.create({
      header: 'การประมูลล้มเหลว',
      message: 'การประมูลสิ้นสุดลงเเล้ว',
      buttons: [
        {
          text: 'ตกลง',
          role: 'success',
          cssClass: 'secondary',
          handler: (blah) => {
            this.auct_type = 'end'
          }
        }
      ]
    })
    await fail_auction.present()
  }

  async sentCongrete(order_id) {
    const alert = await this.alert.create({
      cssClass: 'my-custom-class',
      message: '<strong>ยืนยันการจัดส่งสินค้า</strong>',
      buttons: [
        {
          text: 'ปิด',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'ยืนยัน',
          handler: () => {
            this.service.postData('sentCongrete', { id: order_id }).then((data: any) => {
              let error: any = { head: 'ยังไม่ถึงเวลาจัดส่ง', content: 'สามารถส่งได้ตั้งเเต่<br>' + data.txt + '<br>เป็นต้นไป' }
              if (this.lang == 'en') {
                error = { head: 'Not yet delivery time', content: 'Send time<br>' + data.txt }
              }
              if (data.status == 'success') {
                error = { head: 'สำเร็จ', content: 'กำลังจัดส่งคอนกรีตไปยังลูกค้า' }
                if (this.lang == 'en') {
                  error = { head: 'sucess', content: 'The concrete is being delivered to the customer.' }
                }
                this.service.fb_set('order')
                this.sentNotificationAuctPost(this.order_head.order_id)
              }
              this.service.presentAlert(error.head, error.content)
              //this.service.Toast(error, 2500, 'bottom')
              this.loadDataOrder()
              this.sentNotificationAuctPost(this.order_head.order_id)
            })
          }
        }
      ]
    });
    await alert.present();
  }


  sentNotificationAuct(data) {
    this.service.getData('notificationCustomer/' + data.order_id).then((notify_c: any) => {
      var member_id = ''
      var titel = ''
      var content = ''
      if (notify_c != '') {
        // some code //
      }
    })
    this.service.getData('notificationSale/' + data.order_id).then((notify_s: any) => {
      var notify = {}
      if (notify_s != '') {
        notify_s.auction_in.forEach((el, i) => {
          // some code //
        });
      }
    })
  }

  sentNotificationAuctPost(order_id) {
    this.service.getData('notificationCustomer/' + order_id).then((notify_c: any) => {
      var member_id = ''
      var titel = ''
      var content = ''
      if (notify_c != '') {
        // some code //
      }
    })
  }

  public slip: any = {
    order: {
      id: null,
      send_date: null
    },
    items:
      [
        { item: { en: null, th: null }, val: null, qty: null, unit: { en: null, th: null } }
      ]
    ,
    total: null,
    location: {
      customer: {
        name: null,
        address: null,
        phone: null,
      },
      sale: {
        name: null,
        email: null,
        phone: null,
        address: null,
      }
    }
  }
  public lang = ''
  loadDetailOrderPassing() {
    this.service.getData('winAuction/' + this.order_head.ap_id).then(async (res: any) => {
      var sale: any = {}
      await this.service.storage_get('data_login').then(user => {
        sale = user
      })

      await this.service.storage_get('lang').then(lang => {
        this.lang = lang
      })

      let roundPass = 1
      if (this.order_data.car == 1) {
        roundPass = Math.ceil(this.order_head.count / 2)
      } else {
        roundPass = Math.ceil(this.order_head.count / 5)
      }

      let order: any = await {
        id: this.order_head.order_id,
        send_date: this.order_head.send_date + ' / ' + this.order_head.send_time.substring(0, 5)
      }

      let sendChk = { item: { en: 'shipping (free)', th: 'ค่าส่ง (ฟรี)' }, val: res.ap_diff_price, qty: roundPass, unit: { en: 'round', th: 'เที่ยว' } }
      if (this.order_head.count < 3) {
        sendChk = { item: { en: 'shipping', th: 'ค่าส่ง' }, val: res.ap_diff_price, qty: roundPass, unit: { en: 'round', th: 'เที่ยว' } }
      }
      let items = await [
        { item: { en: this.order_head.p_name.en, th: this.order_head.p_name.th }, val: res.ap_price, qty: Number(this.order_head.count), unit: { en: 'cube', th: 'คิว' } },
        sendChk
      ]

      let total = 0
      items.forEach(item => {
        total += item.val * item.qty
      });
      // console.log(res);

      let subdistrict = await ' ตำบล ' + this.order_head.subdistrict_th
      let district = await ', อำเภอ ' + this.order_head.district_th
      let province = await ', จังหวัด ' + this.order_head.province_th
      if (this.lang == 'en') {
        subdistrict = ' Tambon ' + this.order_head.subdistrict_en
        district = ', Amphur ' + this.order_head.district_en
        province = ', ' + this.order_head.province_en
      }

      let location = await {
        customer: {
          name: this.order_head.name,
          address: this.order_head.address + subdistrict + district + province,
          phone: this.order_head.phone
        },
        sale: {
          name: sale.name,
          email: sale.email,
          phone: sale.phone,
          address: sale.address,
        }
      }

      this.slip = await {
        order: order,
        items: items,
        total: total,
        location: location
      }

      // set date format //
      var day = this.slip.order.send_date.substring(0, 4)
      var mount = this.slip.order.send_date.substring(5, 7)
      var year = this.slip.order.send_date.substring(8, 11)
      var time = this.slip.order.send_date.substring(13, 18)
      this.slip.order.send_date = day + '/' + mount + '/' + year + ' | ' + time

    })
  }

  ngOnDestroy() {
    this.statePage = false
  }

  close() {
    this.modal.dismiss({ value: 'xxx' });
  }
}
