import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AuctionPageRoutingModule } from './auction-routing.module';

import { AuctionPage } from './auction.page';
import { TranslateModule } from '@ngx-translate/core';
//import { VerifyComponentModule } from 'src/app/verify-component/verify-component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule.forChild(),
    AuctionPageRoutingModule,
    //VerifyComponentModule
  ],
  declarations: [AuctionPage]
})
export class AuctionPageModule {}
