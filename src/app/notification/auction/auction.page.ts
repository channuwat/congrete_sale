import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { WebapiServiceProvider } from 'src/app/providers/webapi-service/webapi-service';
import { Storage } from '@ionic/storage';
import { AngularFireDatabase } from '@angular/fire/database';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { AuctionPage as AuctionModal } from '../../auction/auction.page';
import { async } from '@angular/core/testing';

@Component({
  selector: 'app-auction',
  templateUrl: './auction.page.html',
  styleUrls: ['./auction.page.scss'],
})
export class AuctionPage implements OnInit {

  public user: any = []
  public statePage = false
  constructor(
    public service: WebapiServiceProvider,
    public fb: AngularFireDatabase,
    public ref: ChangeDetectorRef,
    public route: Router,
    public modalController: ModalController
  ) {
    this.user.approve = null
  }

  ngOnInit() {
    this.service.storage_get('data_login').then(async (data_u: any) => {
      this.user = await data_u
    })
  }

  ionViewWillEnter() {
    setTimeout(() => {
      this.statePage = true
      this.service.fb_val('notification/user_' + this.user.member_id + '/alert', (state) => {
        this.service.withoutStoreUser(this.user.member_id).then((user: any) => {
          this.user = user.data
        })
        if (this.statePage) {
          this.loadListAuction(this.user.member_id)
        }
      })
    }, 500);
  }


  public listAlert: any = []
  public Lang: string = ""
  public page = 0
  loadListAuction(member_id) {
    this.service.getData('getListAlertAuction/' + member_id + '/' + this.page).then((auct_data) => {
      this.service.storage_get('lang').then(lang => {
        this.Lang = lang
      })
      this.listAlert = auct_data
      this.ref.detectChanges()
    })

  }

  ionViewDidLeave() {
    this.statePage = false
  }

  Route(uri) {
    this.route.navigate(['../' + uri])
  }

  doRefresh(event) {
    this.page = 0
    this.service.getData('getListAlertAuction/' + this.user.member_id + '/' + this.page).then((auct_data) => {
      this.service.storage_get('lang').then(lang => {
        this.Lang = lang
      })
      this.listAlert = auct_data
      this.ref.detectChanges()
    })
    setTimeout(() => {
      event.target.complete();
    }, 1000);
  }

  loadData(event) {
    this.page += 10
    this.service.getData('getListAlertAuction/' + this.user.member_id + '/' + this.page).then((auct_data: any) => {
      auct_data.forEach(val => {
        this.listAlert.push(val)
      });
      this.ref.detectChanges()
    })
    setTimeout(() => {
      event.target.complete();
    }, 2000);
  }

  async selectAuction(id, type) {
    this.service.postData('readNotification', { order_id: id, member_id: this.user.member_id }).then(async () => {
      await this.loadListAuction(this.user.member_id)
      const modal = await this.modalController.create({
        component: AuctionModal,
        componentProps: {
          order_data: { order_id: id, auct_type: type }
        }
      });
      modal.onDidDismiss().then((res: any) => {
      });
      return await modal.present();
    })
  }


}
