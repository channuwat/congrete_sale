import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SystemModalPage } from './system-modal.page';

describe('SystemModalPage', () => {
  let component: SystemModalPage;
  let fixture: ComponentFixture<SystemModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SystemModalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SystemModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
