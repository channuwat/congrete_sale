import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SystemModalPage } from './system-modal.page';

const routes: Routes = [
  {
    path: '',
    component: SystemModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SystemModalPageRoutingModule {}
