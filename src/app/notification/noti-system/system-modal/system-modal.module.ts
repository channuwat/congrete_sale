import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SystemModalPageRoutingModule } from './system-modal-routing.module';

import { SystemModalPage } from './system-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SystemModalPageRoutingModule
  ],
  declarations: [SystemModalPage]
})
export class SystemModalPageModule {}
