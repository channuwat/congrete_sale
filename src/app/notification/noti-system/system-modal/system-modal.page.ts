import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-system-modal',
  templateUrl: './system-modal.page.html',
  styleUrls: ['./system-modal.page.scss'],
})
export class SystemModalPage implements OnInit {
  @Input() noti: any
  constructor(public modalController: ModalController) { }

  ngOnInit() {
  }

  dismiss() {
    this.modalController.dismiss({
      'dismissed': true
    });
  }
}
