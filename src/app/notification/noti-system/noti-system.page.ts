import { NotificationDetailPage } from './../../notification-detail/notification-detail.page';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { WebapiServiceProvider } from 'src/app/providers/webapi-service/webapi-service';
import { AngularFireDatabase } from '@angular/fire/database';
import { Router } from '@angular/router';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player/ngx';
import { SystemModalPage } from './system-modal/system-modal.page';

@Component({
  selector: 'app-noti-system',
  templateUrl: './noti-system.page.html',
  styleUrls: ['./noti-system.page.scss'],
})
export class NotiSystemPage implements OnInit {

  public user: any = []
  public statePage = false
  constructor(
    public service: WebapiServiceProvider,
    public fb: AngularFireDatabase,
    public ref: ChangeDetectorRef,
    public route: Router,
    public modalController: ModalController,
    public youtube : YoutubeVideoPlayer
  ) {
    this.user.approve = null
  }

  ngOnInit() {
    this.service.storage_get('data_login').then(async (data_u: any) => {
      this.user = await data_u
    })
  }

  ionViewWillEnter() {
    setTimeout(() => {
      this.statePage = true
      this.service.fb_val('notification/user_' + this.user.member_id + '/alert', (state) => {
        this.service.withoutStoreUser(this.user.member_id).then((user: any) => {
          this.user = user.data
        })
        if (this.statePage) {
          this.loadListAuction(this.user.member_id)
        }
      })
    }, 500);
  }

  public listAlert: any = []
  public Lang : string = ""
  loadListAuction(member_id) {
    this.service.getData('getListAlertSystem/' + member_id).then((sys_data) => {
      this.service.storage_get('lang').then(lang => {
        this.Lang = lang
      })
      this.listAlert = sys_data
      this.ref.detectChanges()
    })
  }

  Route(uri) {
    this.route.navigate(['../' + uri])
  }

  // async selectAuction(id, type) {
  //   this.service.postData('readNotification', { order_id: id, member_id: this.user.member_id }).then(async () => {
  //     await this.loadListAuction(this.user.member_id)
  //     const modal = await this.modalController.create({
  //       component: AuctionModal,
  //       componentProps: {
  //         order_data: { order_id: id, auct_type: type }
  //       }
  //     });
  //     modal.onDidDismiss().then((res: any) => {
  //     });
  //     return await modal.present();
  //   })
  // }
  openYoutube(url){
    this.youtube.openVideo(url)
  }

  async select(data) {
    const modal = await this.modalController.create({
      component: SystemModalPage,
      componentProps: {noti : data},
      mode : 'ios'
    });
    return await modal.present();
  }
}
