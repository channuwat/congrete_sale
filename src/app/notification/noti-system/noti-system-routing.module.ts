import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NotiSystemPage } from './noti-system.page';

const routes: Routes = [
  {
    path: '',
    component: NotiSystemPage
  },  {
    path: 'system-modal',
    loadChildren: () => import('./system-modal/system-modal.module').then( m => m.SystemModalPageModule)
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NotiSystemPageRoutingModule {}
