import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NotiSystemPageRoutingModule } from './noti-system-routing.module';

import { NotiSystemPage } from './noti-system.page';
import { SystemModalPage } from './system-modal/system-modal.page';
import { SystemModalPageModule } from './system-modal/system-modal.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NotiSystemPageRoutingModule,
    SystemModalPageModule
  ],
  declarations: [NotiSystemPage],
  entryComponents : [SystemModalPage]
})
export class NotiSystemPageModule {}
