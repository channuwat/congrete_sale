import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router, RouterLinkActive, ActivatedRoute } from '@angular/router';
import { Events } from '@ionic/angular';
import { WebapiServiceProvider } from '../providers/webapi-service/webapi-service';


@Component({
  selector: 'app-notification',
  templateUrl: './notification.page.html',
  styleUrls: ['./notification.page.scss'],
})
export class NotificationPage implements OnInit {
  selected = 0;
  constructor(public router: Router, public activatedRoute: ActivatedRoute, public api: WebapiServiceProvider, public ref: ChangeDetectorRef, public events: Events) {
    this.events.subscribe('user:lang', (res) => {
      setTimeout(() => {
        this.ref.detectChanges();
      }, 1000);
    });
  }

  ngOnInit() {

  }

  ionViewWillEnter() {
    this.api.checkUserUnknow().then((res) => {
      let unknow = res
      if (unknow == 0) {
        this.router.navigate(['unknow'])
      }
    })
  }

  page(uri) {
    this.router.navigate(['/main/notification/' + uri]);
    if (uri == 'auction') {
      this.selected = 0;
    } else if (uri == 'noti-system') {
      this.selected = 1;
    }
  }
}
