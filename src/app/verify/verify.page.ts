import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Storage } from '@ionic/storage';
import { WebapiServiceProvider } from '../providers/webapi-service/webapi-service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AlertController } from '@ionic/angular';
import { Location } from '@angular/common';

@Component({
  selector: 'app-verify',
  templateUrl: './verify.page.html',
  styleUrls: ['./verify.page.scss'],
})
export class VerifyPage implements OnInit {

  fileForm = new FormGroup({
    file_1: new FormControl(''),
    temp_1: new FormControl(''),
    file_2: new FormControl(''),
    temp_2: new FormControl(''),
    file_3: new FormControl(''),
    temp_3: new FormControl(''),
  });

  fileMultipleForm = new FormGroup({
    _files1: new FormControl(''),
    _files2: new FormControl(''),
    _files3: new FormControl(''),
    resources_1: new FormControl(''),
    resources_2: new FormControl(''),
    resources_3: new FormControl(''),
  });


  constructor(
    public route: Router,
    public router: ActivatedRoute,
    public store: Storage,
    public service: WebapiServiceProvider,
    public ref: ChangeDetectorRef,
    public alert: AlertController,
    public location: Location
  ) { }

  public user_tmp: any = []
  public user: any = []
  ngOnInit() {
    this.store.get('data_login').then((res: any) => {
      this.user_tmp = res
      this.loadFileVerify(this.user_tmp.member_id)
    })
  }

  public lang = 'th'
  ionViewDidEnter() {
    this.service.storage_get('lang').then((lang_data) => {
      this.lang = lang_data
    })
  }

  public API_case = 'insert'
  public obj_old: any = []
  loadFileVerify(member_id) {
    this.service.getData('getFileVerify/' + member_id).then((files: any) => {
      if (files.files.length > 0) {
        this.API_case = 'update'
      } else {
        this.API_case = 'insert'
      }
      this.obj_old = files.files
      this.user = files.user
      for (let obj of this.obj_old) {
        obj.short_name = obj.picture.slice(56)
        if (obj.comment != '' && obj.status == -1) {
          obj.comment = 'ตรวจสอบไม่ผ่าน : ' + obj.comment
        } else if (obj.comment == '' && obj.status == 0) {
          obj.comment = 'รอตรวจสอบ'
        } else {
          obj.comment = 'ตรวจสอบผ่าน'
        }
      }
    })
  }

  public reader: any
  public satateUpload = false
  checkUpload() {
    this.satateUpload = false
    var empty = [
      this.fileForm.value.file_1,
      this.fileForm.value.file_2,
      this.fileForm.value.file_3
    ]

    for (let e of empty) {
      if (e != '') {
        this.satateUpload = true
      }
    }
  }


  async submit() {
    if (this.satateUpload) {
      let ms = 'ต้องการยืนยันตัวตนหรือไม่!'
      let cancel = 'ยกเลิก'
      let confirm = 'ยืนยัน'
      if (this.lang == 'en') {
        ms = 'Wold you like confirm!'
        cancel = 'cancel'
        confirm = 'confirm'
      }
      const alert = await this.alert.create({
        cssClass: 'my-custom-class',
        message: ms,
        buttons: [
          {
            text: cancel,
            role: 'cancel',
            cssClass: 'secondary',
            handler: (blah) => {
              console.log('Confirm Cancel: blah');
            }
          }, {
            text: confirm,
            handler: () => {
              this.service.postData('uploadVerify', { img: this.fileForm.value, user: this.user_tmp }).then(res => {
                location.reload();
              })
            }
          }
        ]
      });
      await alert.present();
    }
  }

  /////////////////////////////////////////////////////
  public nameFile = [
    { type: 1, name: [], part_temp: [], part: { resources_1: [] } },
    { type: 2, name: [], part_temp: [], part: { resources_2: [] } },
    { type: 3, name: [], part_temp: [], part: { resources_3: [] } },
  ]
  public countFile = 0
  public url_1 = []
  public url_2 = []
  public url_3 = []
  changeFiles(e, type) {
    this.countFile = 0
    if (type == 1) {
      this.url_1 = []
    } else if (type == 2) {
      this.url_2 = []
    } else {
      this.url_3 = []
    }

    for (var t of this.nameFile) {
      if (t.type == type) {
        t.name = []
        t.part_temp = []
        for (var file of e.target.files) {
          t.name.push(file.name)
        }
        var Files = e.target.files
      }
    }

    for (let file of Files) {
      var reader = new FileReader();
      var file_select = this.nameFile[type - 1]
      reader.onload = (event: any) => {
        var newName = 'name:user' + this.user_tmp.member_id + '_type' + type + '_,'
        var string = newName + event.target.result

        file_select.part_temp.push(string)
        if (type == 1) {
          this.url_1.push(event.target.result)
          file_select.part = { resources_1: [] }
          file_select.part = { resources_1: file_select.part_temp }
        } else if (type == 2) {
          this.url_2.push(event.target.result)
          file_select.part = { resources_2: [] }
          file_select.part = { resources_2: file_select.part_temp }
        } else {
          this.url_3.push(event.target.result)
          file_select.part = { resources_3: [] }
          file_select.part = { resources_3: file_select.part_temp }
        }
        this.fileMultipleForm.patchValue(file_select.part);
      }
      reader.readAsDataURL(file);
    }
    setTimeout(() => {
      this.countFile += this.fileMultipleForm.value.rseources_1
      this.countFile += this.fileMultipleForm.value.rseources_2
      this.countFile += this.fileMultipleForm.value.rseources_3
    }, 500);
  }

  async upload(Case) {
    let ms = 'ต้องการยืนยันตัวตนหรือไม่!'
    let cancel = 'ยกเลิก'
    let confirm = 'ยืนยัน'
    if (this.lang == 'en') {
      ms = 'Wold you like confirm!'
      cancel = 'cancel'
      confirm = 'confirm'
    }
    const alert = await this.alert.create({
      cssClass: 'my-custom-class',
      message: ms,
      buttons: [
        {
          text: cancel,
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: confirm,
          handler: () => {
            if (Case == 'insert') {
              this.service.postData('uploadVerify', { img: this.fileMultipleForm.value, user: this.user_tmp }).then(res => {
                this.user = res
              }).then(() => {
                location.reload();
              })
            } else {
              this.service.postData('uploadVerifyEdit', { img: this.fileMultipleForm.value, obj_files: this.obj_old, user: this.user_tmp }).then(res => {
                this.user = res
              }).then(() => {
                location.reload();
              })
            }
          }
        }
      ]
    });
    await alert.present();
  }

  close() {
    //this.route.navigate(['../main/account'])
    this.location.back();
  }

}
