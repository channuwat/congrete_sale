import { ChangeDetectorRef, Component } from '@angular/core';
import { Plugins } from '@capacitor/core';
//const { SplashScreen } = Plugins;
import { TranslateService, LangChangeEvent } from '@ngx-translate/core';
import { FCMPluginOnIonic } from 'cordova-plugin-fcm-with-dependecy-updated/ionic';
// import { FCMPluginOnIonic } from 'cordova-plugin-fcm-with-dependecy-updated/ionic';
import { Platform, ModalController } from '@ionic/angular';
import { Device } from '@ionic-native/device/ngx';
import { Network } from '@ionic-native/network/ngx';
import { Storage } from '@ionic/storage';
import { WebapiServiceProvider } from './providers/webapi-service/webapi-service';
import { Router } from '@angular/router';
import { MobileAccessibility } from '@ionic-native/mobile-accessibility/ngx';
import { AuctionPage } from './auction/auction.page';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { Events } from '@ionic/angular';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: [
    './side-menu/styles/side-menu.scss',
    './side-menu/styles/side-menu.shell.scss',
    './side-menu/styles/side-menu.responsive.scss'
  ]
})

export class AppComponent {
  public userDeviceData = { uuid: "", token: "", platform: "", version: "", member_id: '0' };
  public userDeviceResponse: any

  appPages = [
    {
      title: 'Categories',
      url: '/app/categories',
      icon: './assets/sample-icons/side-menu/categories.svg'
    },
    {
      title: 'Profile',
      url: '/app/user',
      icon: './assets/sample-icons/side-menu/profile.svg'
    },
    {
      title: 'Contact Card',
      url: '/contact-card',
      icon: './assets/sample-icons/side-menu/contact-card.svg'
    },
    {
      title: 'Notifications',
      url: '/app/notifications',
      icon: './assets/sample-icons/side-menu/notifications.svg'
    }
  ];

  accountPages = [
    {
      title: 'Log In',
      url: '/auth/login',
      icon: './assets/sample-icons/side-menu/login.svg'
    },
    {
      title: 'Sign Up',
      url: '/auth/signup',
      icon: './assets/sample-icons/side-menu/signup.svg'
    },
    {
      title: 'Tutorial',
      url: '/walkthrough',
      icon: './assets/sample-icons/side-menu/tutorial.svg'
    },
    {
      title: 'Getting Started',
      url: '/getting-started',
      icon: './assets/sample-icons/side-menu/getting-started.svg'
    },
    {
      title: '404 page',
      url: '/page-not-found',
      icon: './assets/sample-icons/side-menu/warning.svg'
    }
  ];

  textDir = 'ltr';

  constructor(public translate: TranslateService,
    public FCM: FCMPluginOnIonic,
    public platform: Platform,
    public device: Device,
    public network: Network,
    public webService: WebapiServiceProvider,
    public storage: Storage,
    public route: Router,
    private mobileAccessibility: MobileAccessibility,
    public modalCtr: ModalController,
    private splashScreen: SplashScreen,
    public events: Events,
    public ref: ChangeDetectorRef
  ) {
    this.events.subscribe('user:lang', (res) => {
      console.log(res);
      if (res == 'th') {
        this.translate.setDefaultLang('th');
      } else if (res == 'en') {
        this.translate.setDefaultLang('en');
      } else {
        this.translate.setDefaultLang('th');
      }
    });

    this.backButtonEvent()
    this.setLanguage();
    // if (this.platform.is('android') === true) {

    this.platform.ready().then(() => {
      this.initializeApp();
      this.mobileAccessibility.usePreferredTextZoom(false);
      this.checkData();
      setTimeout(() => {
        this.fcmNotify();
      }, 300);
    });
    // }
  }

  ngOnInit() {

  }

  ionViewWillEnter() {
  }

  async initializeApp() {
    // try {
    //   await SplashScreen.hide();
    // } catch (err) {
    //   console.log('This is normal in a browser', err);
    // }
    // เช็คว่ามีการเชื่อมต่อ network หรือยัง
    if (this.network.type !== "none") {
      /*
      *---------------------------------------------------------------
      *  PUSH NOTIFICATION WITH FIREBASE
      *---------------------------------------------------------------
      */
      // ลงทะเบียน  device เพื่อรับ Token
      setTimeout(() => {
        this.FCM.subscribeToTopic('all');

        if(this.platform.is('ios')){
          this.FCM.requestPushPermission().then(()=>{
            this.FCM.getToken().then(token=>{
              this.userDeviceData.token = token;
            // Device info
            this.userDeviceData.uuid = this.device.uuid;
            this.userDeviceData.platform = this.device.platform;
            this.userDeviceData.version = this.device.version;
            console.log(this.userDeviceData);
            })
          })
        }
        else{
        this.FCM.getToken().then(token => {
          if (!this.platform.is('desktop')) {
            this.userDeviceData.token = token;
            // Device info
            this.userDeviceData.uuid = this.device.uuid;
            this.userDeviceData.platform = this.device.platform;
            this.userDeviceData.version = this.device.version;
            console.log(this.userDeviceData);
          } else {
            this.userDeviceData.token = '';
            // Device info
            this.userDeviceData.uuid = 'browser';
            this.userDeviceData.platform = 'web';
            this.userDeviceData.version = '1';
          }
          this.storage.get("data_login").then((data) => {
            if (data) {
              this.userDeviceData.member_id = data.member_id;
              this.webService.postData('register_device', this.userDeviceData).then((result) => {
                console.log(result + ' ***1*** ');
                if (result) {
                  this.userDeviceResponse = result;
                  if (this.userDeviceResponse.Sucess) {
                    console.log('Regis new device success');
                    //this.webService.Toast('Regis new device success', 2500, 'middle');
                    // Keep UUID to localStorage
                    this.storage.set('uuid', this.device.uuid);
                  } else {
                    console.log('Fail! Cannot Regis new device');
                    //this.webService.Toast('Fail! Cannot Regis new device', 2500, 'middle');
                  }
                }

              }, (error) => {
                //console.log(error);
                if (error.status == 0) {
                  console.log('Error status Code:0 Web API Offline');
                  //this.webService.Toast('Error status Code:0 Web API Offline', 2500, 'middle');
                }
              });
            } else {
              this.userDeviceData.member_id = '0';
              this.webService.postData('register_device', this.userDeviceData).then((result) => {
                console.log(result + ' ***2*** ');
                if (result) {
                  this.userDeviceResponse = result;
                  if (this.userDeviceResponse.Sucess) {
                    console.log('Regis new device success');
                    //this.webService.Toast('Regis new device success', 2500, 'middle');
                    // Keep UUID to localStorage
                    this.storage.set('uuid', this.device.uuid);
                  } else {
                    console.log('Fail! Cannot Regis new device');
                    //this.webService.Toast('Fail! Cannot Regis new device', 2500, 'middle');
                  }
                }
              }, (error) => {
                //console.log(error);
                if (error.status == 0) {
                  console.log('Error status Code:0 Web API Offline');
                  //this.webService.Toast('Error status Code:0 Web API Offline', 2500, 'middle');
                }
              });
            }
          });
          // Connect Web API


        }); // then
      }
        // อัพเดท Token
        this.FCM.onTokenRefresh().subscribe(token => {
          this.userDeviceData.token = token;
          // Device info
          this.userDeviceData.uuid = this.device.uuid;
          this.userDeviceData.platform = this.device.platform;
          this.userDeviceData.version = this.device.version;

          // Connect Web API
          this.webService.postData('register_device', this.userDeviceData).then((result) => {
            console.log(result + ' ***3*** ');
            if (result) {
              this.userDeviceResponse = result;
              if (this.userDeviceResponse.Sucess) {
                //this.webService.Toast('Regis new device success', 2500, 'middle');
                console.log('Regis new device success');

                // Keep UUID to localStorage
                this.storage.set('uuid', this.device.uuid);

              } else {
                console.log('Fail! Cannot Regis new device');
                //this.webService.Toast('Fail! Cannot Regis new device', 2500, 'middle');
              }
            }

          }, (error) => {
            //console.log(error);
            if (error.status == 0) {
              console.log('Error status Code:0 Web API Offline');
              //this.webService.Toast('Error status Code:0 Web API Offline', 2500, 'middle');
            }
          });
        });
      }, 600);
    } else if (this.network.type === 'none') {
      this.webService.presentAlert('error', 'not_internet');
    }
  }

  setLanguage() {
    this.webService.storage_get('lang').then((res: any) => {
      if (!res) {
        this.translate.setDefaultLang('th');
        this.webService.storage_set('lang', 'th')
      } else {
        if (res == 'th') {
          this.translate.setDefaultLang('th');
        } else if (res == 'en') {
          this.translate.setDefaultLang('en');
        } else {
          this.translate.setDefaultLang('th');
        }
      }
    })
  }

  async fcmNotify() {
    this.FCM.onNotification().subscribe(async (res: any) => {
      console.log(res);
      if (res.wasTapped) {
        console.log('Tapped');
        try {
          const el = await this.modalCtr.getTop()
          if (el) {
            el.dismiss()
            //return
          }
        } catch (error) {

        }
        if (res.click_action == 'chat') {
          console.log('chat');
          this.route.navigate(['main/tab3']);
        }

        if (res.click_action == 'neworder') {
          console.log('auction');
          this.route.navigate(['main/tab2/order-wait']);
          setTimeout(async () => {
            let order_data = {
              order_id: res.order_id,
              auct_type: 'auction'
            }
            const modal = await this.modalCtr.create({
              component: AuctionPage,
              componentProps: {
                order_data: { order_id: res.order_id, auct_type: 'auction' }
              }
            });
            return await modal.present();
          }, 1000);
        }

        if (res.click_action == 'auction') {
          console.log('auction');
          this.route.navigate(['main/tab2/order-wait-send']);
          setTimeout(async () => {
            let order_data = {
              order_id: res.order_id,
              auct_type: 'auction'
            }
            console.log(order_data);

            const modal = await this.modalCtr.create({
              component: AuctionPage,
              componentProps: {
                order_data: { order_id: res.order_id, auct_type: 'auction' }
              }
            });
            return await modal.present();
          }, 1000);
        }

        if (res.click_action == 'winner') {
          console.log('auction');
          this.route.navigate(['main/tab2/order-send']);
          setTimeout(async () => {
            let order_data = {
              order_id: res.order_id,
              auct_type: 'end'
            }
            const modal = await this.modalCtr.create({
              component: AuctionPage,
              componentProps: {
                order_data: { order_id: res.order_id, auct_type: 'end' }
              }
            });
            return await modal.present();
          }, 1000);
        }

        if (res.click_action == 'accecpt') {
          console.log('accecpt');
          this.route.navigate(['main/tab2/order-send']);
          setTimeout(async () => {
            let order_data = {
              order_id: res.order_id,
              auct_type: 'end'
            }
            const modal = await this.modalCtr.create({
              component: AuctionPage,
              componentProps: {
                order_data: { order_id: res.order_id, auct_type: 'end' }
              }
            });
            return await modal.present();
          }, 1000);
        }
      }
      console.log('NoTap');
    });
  }

  public subscribe: any
  public lastTimeBackPress = 0;
  public timePeriodToExit = 2000;
  backButtonEvent() {

    this.subscribe = this.platform.backButton.subscribeWithPriority(666666, async () => {
      try {
        const element = await this.modalCtr.getTop();
        if (element) {
          element.dismiss();
          return;
        }
      } catch (error) {
        console.log(error);

      }

      var loginArray = [
        '/auth/forgot-password',
        '/auth/signup'
      ]

      var signUpArray = [
        'auth/confirm-password;confirm=signup',
      ]

      var forgotPassArray = [
        'auth/confirm-password;confirm=forgot',
        '/auth/add-password/login',
      ]

      var unknowArray = [
        '/tab3',
        '/verify'
      ]

      var HomeArray = [
        '/main/account/setting-location-order;page=home',
        '/main/tab3',
        '/main/notification/auction',
        '/main/notification/noti-system',
        '/main/account',
        '/main/how'
      ]


      var AccountArray = [
        '/main/account/config-account;member_id=' + this.userDeviceData.member_id,
        '/main/account/score;member_id=' + this.userDeviceData.member_id,
        '/auth/add-password/account',
        //'/main/account/config-account;member_id=' + this.userDeviceData.member_id,
        '/main/account/setting-location-order',
        '/main/account/setting-location-order;page=account',
        '/main/account/book-bank',
        '/main/account/verify',
        '/main/account/score;member_id=' + this.userDeviceData.member_id
      ]

      var LocationArray = [
        '/main/account/setting-location-order;page=account/add-location',
        '/main/account/setting-location-order;page=home/add-location'
      ]
      console.log(this.route.url);
      var routeSignUp = signUpArray.indexOf(this.route.url)
      var routeForgotPass = forgotPassArray.indexOf(this.route.url)
      var routeLogin = loginArray.indexOf(this.route.url)
      var routeUnkonw = unknowArray.indexOf(this.route.url)
      var routeHome = HomeArray.indexOf(this.route.url)
      var routeAccount = AccountArray.indexOf(this.route.url)
      var routeLocation = LocationArray.indexOf(this.route.url)

      if (routeLogin != -1) {
        this.route.navigate(['/auth/login']);
      }
      if (routeSignUp != -1) {
        this.route.navigate(['/auth/signup']);
      }
      if (routeForgotPass != -1) {
        this.route.navigate(['/auth/forgot-password']);
      }
      if (routeUnkonw != -1) {
        this.route.navigate(['/unknow']);
      }
      if (routeHome != -1) {
        this.route.navigate(['/main/tab2']);
      }
      if (routeAccount != -1) {
        this.route.navigate(['/main/account']);
      }
      if (routeLocation != -1) {
        this.route.navigate(['/main/account/setting-location-order']);
      }

      if (this.route.url == '/main/tab2/order-wait' || this.route.url == '/auth/change-password') {
        if (new Date().getTime() - this.lastTimeBackPress >= this.timePeriodToExit) {
          this.lastTimeBackPress = new Date().getTime();
          this.webService.Toast('กด Back อีกครั้ง เพื่อออกจาก App', 2000, 'bottom')
        } else {
          navigator["app"].exitApp();
        }
      }
    })
  }

  async checkData() {
    await this.storage.get('walkthrough').then(async (res: any) => {
      if (!res) {
        await this.route.navigate(['walkthrough']);
      }
    })
    setTimeout(() => {
      this.splashScreen.hide();
    }, 1000);

  }

}
