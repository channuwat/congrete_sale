import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { VerifyComponentComponent } from './verify-component.component';
import { TranslateModule } from '@ngx-translate/core';
import { VerifyComponentRoutingModule } from './verify-component-routing.module';



@NgModule({
  declarations: [VerifyComponentComponent],
  imports: [
    CommonModule,
    IonicModule,
    TranslateModule.forChild(),
    VerifyComponentRoutingModule
  ],
  exports: [
    VerifyComponentComponent
  ]
})
export class VerifyComponentModule { }
