import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VerifyComponentComponent } from './verify-component.component';

const routes: Routes = [
  {
    path: '',
    component: VerifyComponentComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VerifyComponentRoutingModule {}
