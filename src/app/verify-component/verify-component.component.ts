import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { WebapiServiceProvider } from '../providers/webapi-service/webapi-service';
import { async } from 'rxjs/internal/scheduler/async';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-verify-component',
  templateUrl: './verify-component.component.html',
  styleUrls: ['./verify-component.component.scss'],
})
export class VerifyComponentComponent implements OnInit {

  constructor(
    public router: Router,
    public service: WebapiServiceProvider,
    public storage: Storage
  ) { }

  ngOnInit() { }

  route(uri) {
    this.router.navigate(['../' + uri])
  }

  profile(type) {
    if (type == 'tab3') {
      this.router.navigate([type])
    } else {
      this.storage.get('data_login').then((data: any) => {
        this.service.withoutStoreUser(data.member_id).then((user: any) => {
          user = user.data
          if (user.approve != 0) {
            this.router.navigate(['main/tab2/order-wait'])
          } else {
            location.reload()
          }
        })
      })

    }
  }

  doRefresh(event) {
    this.service.storage_get('data_login').then(async (login: any) => {
      await this.service.withoutStoreUser(login.member_id).then(async (user: any) => {
        user = user.data
        if (user.approve == 1) {
          this.router.navigate(['main/tab2/order-wait'])
        }else{
          location.reload()
        }
      })
    })
  }
}
