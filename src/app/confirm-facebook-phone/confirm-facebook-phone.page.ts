import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { WebapiServiceProvider } from '../providers/webapi-service/webapi-service';
import { Facebook } from '@ionic-native/facebook/ngx';
import { Location } from '@angular/common';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-confirm-facebook-phone',
  templateUrl: './confirm-facebook-phone.page.html',
  styleUrls: ['./confirm-facebook-phone.page.scss'],
})
export class ConfirmFacebookPhonePage implements OnInit {
  public formCtr: FormGroup
  constructor(
    public store: Storage,
    public service: WebapiServiceProvider,
    public facebook: Facebook,
    public loccal: Location,
    public router: Router
  ) {
    this.formCtr = new FormGroup({
      'tel': new FormControl('', Validators.compose([
        Validators.required, Validators.minLength(9), Validators.maxLength(10),
        Validators.pattern('0+[0-9]+')
      ]))
    });
  }

  ngOnInit() {
    this.proFile()
  }

  public profile = { fb_id: null, email: null, name: null, picture: null, }
  proFile() {
    this.store.get('fbuser').then(res => {
      this.profile = { fb_id: res.id, email: res.email, name: res.name, picture: res.picture }
    })
  }

  public error = { status: 0, txt: [], typeCF: 0 }
  hasPhone(phone) {
    this.error = { status: 0, txt: [], typeCF: 0 }
    if (phone.length < 9) {
      this.error.status = 1
      this.error.txt.push('โปรกรอกอย่างน้อย 9 ตัวอักษร')
    }
    if (this.formCtr.status == 'INVALID') {
      this.error.status = 1
      this.error.txt.push('เบอร์โทรไม่ถูกต้อง')
    }
    if (this.formCtr.status == 'VALID' && phone.length > 8) {
      this.service.getData('checkPhoneFacebook/' + phone).then((has: any) => {
        if (has.state == 0) {
          this.error.status = 2
          this.error.txt.push(has.errors)
          this.error.typeCF = has.in_db
        } else if (has.state == 1) {
          this.error.status = 1
          this.error.txt.push(has.errors)
        }
      })

    }

  }

  registerWithFacebook(phone, type, typeCF) {
    this.service.postData('add_otpWithFacebook', { tel: phone, type: type }).then((res: any) => {
      console.log(res);
      if (res.flag == '0') {
        this.service.Toast(res.error, 2000, 'bottom');
      } else {
        this.service.storage_set("otp_phone", phone);
        this.service.storage_set("otp_code", res.code);
        this.service.Toast("ระบบได้ส่ง SMS ไปยังเบอร์ของคุณเรียบร้อยแล้ว", 2000, 'bottom');
        setTimeout(() => {
          this.store.remove('phone_confirm')
          this.store.set('phone_confirm', this.formCtr.value.tel)
          if (typeCF == 'invalid') {
            this.router.navigate(['auth/confirm-password/', { confirm: 'facebook' }]);
          } else {
            this.router.navigate(['auth/confirm-password/', { confirm: 'facebook-phone' }]);
          }

        }, 2000);
      }
    });

  }

  back() {
    this.loccal.back()
  }

}
