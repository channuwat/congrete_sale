import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConfirmFacebookPhonePageRoutingModule } from './confirm-facebook-phone-routing.module';

import { ConfirmFacebookPhonePage } from './confirm-facebook-phone.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConfirmFacebookPhonePageRoutingModule,
    ReactiveFormsModule,
    TranslateModule.forChild()
  ],
  declarations: [ConfirmFacebookPhonePage]
})
export class ConfirmFacebookPhonePageModule {}
