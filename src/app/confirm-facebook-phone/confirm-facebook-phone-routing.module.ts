import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConfirmFacebookPhonePage } from './confirm-facebook-phone.page';

const routes: Routes = [
  {
    path: '',
    component: ConfirmFacebookPhonePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConfirmFacebookPhonePageRoutingModule {}
