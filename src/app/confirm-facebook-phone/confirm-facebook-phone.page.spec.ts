import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ConfirmFacebookPhonePage } from './confirm-facebook-phone.page';

describe('ConfirmFacebookPhonePage', () => {
  let component: ConfirmFacebookPhonePage;
  let fixture: ComponentFixture<ConfirmFacebookPhonePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfirmFacebookPhonePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ConfirmFacebookPhonePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
