// import { Http, Headers } from '@angular/http';
// import { HttpClient, HttpHeaders, HttpErrorResponse,HttpRequest } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastController, LoadingController, AlertController } from '@ionic/angular';
// import { GlobalProvider } from '../global/global';
import { Storage } from '@ionic/storage';
import { AngularFireDatabase } from '@angular/fire/database';
import { resolve } from 'core-js/fn/promise';
import { Router } from '@angular/router';
import { async } from 'rxjs/internal/scheduler/async';
// import { TranslateService } from '@ngx-translate/core';
// import { AngularFireDatabase } from '@angular/fire/database';

@Injectable()
export class WebapiServiceProvider {
  // public base_url = "https://api2.concretedeliveryeasy.com/index.php/sale_app/";
  public base_url = "https://api.concretedeliveryeasy.com/index.php/sale_app_106/";

  constructor(
    private toast: ToastController,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public http: HttpClient,
    public firebase: AngularFireDatabase,
    private storage: Storage,
    private route: Router
    // private translate: TranslateService,
    // public firebase: AngularFireDatabase,
    // private global: GlobalProvider
  ) {
    //
  }

  async presentAlert(header, message) {
    const alert = await this.alertCtrl.create({
      header: header,
      message: message,
      buttons: ['OK']
    });
    return await alert.present();
  }

  async presentToast() {
    // const toast = await this.toast.create({
    //   message: this.global.msgStatusCodeAPI_0,
    //   duration: 3000,
    //   position: 'top'
    // });
    // toast.present();
  }
  async Toast(text, second, position) {
    const toast = await this.toast.create({
      message: text,
      position: position,
      duration: second
    });
    toast.present();
  }
  on_loading() {
    // this.translate.get('wait').subscribe(wait => {
    //   this.on_loading2(wait);
    // });
  }
  async on_loading2(wait) {
    let loading = await this.loadingCtrl.create({
      message: wait,
      spinner: 'crescent',
      duration: 2000
    });
    return await loading.present();
  }
  // POST Method
  storage_set(key, val) {
    this.storage.set(key, val);
  }
  storage_get(key) {
    return this.storage.get(key);
  }

  // ถ้า Dev ให้เปิดตัวบน ถ้าอัพ Production ให้เปิดตัวล่าง
  // public fb_part = 'dev/'
  public fb_part = ''
  fb_set_alert(child) {
    this.firebase.database.ref(this.fb_part + child).set(0);
  }
  fb_set(child) {
    this.firebase.database.ref(this.fb_part + child).set(Math.ceil(Math.random() * 100));
  }
  fb_val(child, func) {
    this.firebase.database.ref(this.fb_part + child).on("value", (val: any) => {
      func(val.node_.value_);
    });
  }
  postData(segment, objdata) {
    return new Promise((resolve, reject) => {
      var headers = new Headers();
      headers.append("Accept", 'application/json');
      headers.append('Content-Type', 'application/json');
      this.storage.get('lang').then((result) => {
        if (!result) {
          result = "th";
        }
        this.http.post(this.base_url + segment + "?lang=" + result, JSON.stringify(objdata))
          .subscribe((res: any) => {
            resolve(res);

          }, (err) => {
            if (err.status == 0) {
              this.presentToast();
              reject(err);
            }
          });
      });
    });
  }

  billcus(order_id) {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      // headers.append('Content-Type', this.global.contentType);
      // headers.append('Authorization', this.global.authKey);
      // headers.append('Accept', 'application/json, text/plain, */*');

      // var headers = new Headers();
      // // headers.append("Accept", 'application/json');
      // // // headers.append('Content-Type', 'application/json');
      // // // let headers = new Headers();
      // headers.append('Content-Type', this.global.contentType);
      // headers.append('Authorization', this.global.authKey);


      // let options = new RequestOptions({ headers: headers });
      // this.http.get(this.global.baseUrlApi + segment, { headers: headers }) 
      // this.storage.get('lang').then((result) => {
      //   this.http.get(this.global.baseUrlApi + "bill_cus/" + order_id + "?lang=" + result, { headers: headers })
      //     .subscribe((res: any) => {
      //       resolve(res._body);
      //     }, (err) => {
      //       if (err.status == 0) {
      //         this.presentToast();
      //         reject(err);
      //       }
      //       reject(err);
      //     });
      // });
    });
  }
  getData(segment) {
    return new Promise((resolve, reject) => {
      this.storage.get('lang').then((result) => {
        this.http.get(this.base_url + segment + "?lang=" + result)
          .subscribe(res => {
            resolve(res);
          }, (err) => {
            if (err.status == 0) {
              this.presentToast();
              reject(err);
            }
            reject(err);
          });
      });
    });
  }

  withoutStoreUser(user_id) {
    return new Promise((resolve, reject) => {
      this.postData('withoutStoreUser', { id: user_id }).then((data: any) => {
        resolve(data)
      })
    });
  }

  async checkUserUnknow() {
    return new Promise((resolve, reject) => {
      var approve = 0
      this.storage.get('data_login').then(async (data) => {
        if (data) {
          await this.withoutStoreUser(data.member_id).then((user: any) => {
            user = user.data
            if (user.approve == 0) {
              approve = 0
              //this.route.navigate(['unknow'])
            } else {
              approve = 1
            }
          })
        }
        resolve(approve);
      })
    })
  }

  lastLocationURI(type) {
    if (type == 'set') {
      this.storage.set('last_location', this.route.url)
    } else {
      this.storage.get('last_location').then(async (uri) => {
        this.route.navigate([uri])
      })
    }
  }
}
