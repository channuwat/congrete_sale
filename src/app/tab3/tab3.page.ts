import { WebapiServiceProvider } from './../providers/webapi-service/webapi-service';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { Storage } from '@ionic/storage';
import { AngularFireDatabase } from '@angular/fire/database';
import { IonContent, IonInfiniteScroll } from '@ionic/angular';
import { Location } from '@angular/common';
@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']

})
export class Tab3Page implements OnInit {
  public chatForm: FormGroup = null;
  public chats = [];
  @ViewChild(IonInfiniteScroll, { static: false }) infiniteScroll: IonInfiniteScroll;
  @ViewChild(IonContent, { static: false }) content: IonContent;
  constructor(public api: WebapiServiceProvider,
    public storage: Storage,
    private ref: ChangeDetectorRef,
    public firebase: AngularFireDatabase,
    public location: Location
  ) {

    this.chatForm = new FormGroup({
      message: new FormControl('')
      //message: new FormControl('', Validators.compose([Validators.required]))
    })
  }

  ionViewDidEnter() {
    setTimeout(() => {
      this.scrollToBottom()
    }, 400);
  }

  scrollToBottom(): void {
    this.content.scrollToBottom(300);
  }

  loadData(event) {
    let page = this.chats.length
    this.api.getData("load_chat/" + this.user.member_id + "/"+page).then((res: any) => {
      res.forEach(val => {
        this.chats.unshift(val)
      });
      console.log(this.chats);
      setTimeout(() => {
        event.target.complete();
        this.ref.detectChanges();
      }, 500);
    })
    
  }

  public user: any = []
  ngOnInit(): void {
    this.storage.get('data_login').then((login: any) => {
      this.user = login
      this.api.fb_val("chat", (val) => {
        this.api.getData("load_chat/" + login.member_id + "/0").then((res: any) => {
          this.chats = res;
          console.log(this.chats);
          setTimeout(() => {
            this.scrollToBottom();
            this.ref.detectChanges();
          }, 550);
        })
      })
    });
  }

  send() {
    let data = this.chatForm.value;
    if (data.message.trim() != '') {
      this.storage.get('data_login').then((login: any) => {
        data.img = null
        data.type = ''
        data.member_id = login.member_id;
        console.log(data);
        
        this.api.postData("add_chat", { data }).then((res: any) => {
          let a = this.api.fb_set('chat');
          this.api.fb_set('alert/menu/chat_sale');
          this.scrollToBottom();
        });

        this.chatForm.setValue({ message: '' });
      });
    }
  }

  public file: File
  public render: any
  public change = 0
  public progress = false
  uploadImg(e) {
    this.progress = true
    this.storage.get('data_login').then((login: any) => {
      let data = this.chatForm.value;
      var type = ''
      this.render = new FileReader()
      this.file = e.target.files[0]
      if (this.file.size <= 8388608) {
        this.render.readAsDataURL(this.file);
        if (this.render != undefined) {
          type = this.file.type
        }

        setTimeout(() => {
          data.img = this.render.result
          data.type = type
          data.member_id = login.member_id;
          this.api.postData('add_chat', { data: data }).then((res: any) => {
            this.progress = false
            let a = this.api.fb_set('chat');
            this.scrollToBottom();
          });
        }, 2000);
        this.chatForm.setValue({ message: '' });
      } else {
        this.progress = false
        this.api.Toast('โปรดเเนบไฟล์ภาพขนาดไม่เกิน 8 Mb.', 3000, 'top')
      }
    });
  }

  close() {
    this.location.back()
  }

  read() {
    this.api.fb_set_alert('notification/user_' + this.user.member_id + '/chat');
    //this.firebase.database.ref('notification/user_' + this.user.member_id + '/chat').set(0)
  }
}
